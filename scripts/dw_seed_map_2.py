import samri.plotting.maps as maps
from samri.fetch.local import roi_from_atlaslabel

stat_map = "data/dw_l2_seed/assignment_4_spherical.nii.gz"
template = "/usr/share/mouse-brain-templates/dsurqec_40micron_masked.nii"

myroi = roi_from_atlaslabel(stat_map,[3],
	save_as='/tmp/dw_seed_assignment_3.nii.gz',
	)

maps.stat3D('/tmp/dw_seed_assignment_3.nii.gz',
	cut_coords=(0.0,-4.6,-3.4),
	scale=0.3,
	alpha=0.75,
	template=template,
	show_plot=False,
	threshold=0.8,
	threshold_mesh=0.8,
	draw_colorbar=False,
	cmap=['#4DAF4A'],
	)
