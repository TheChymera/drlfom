import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
from os import path
from samri.typesetting import inline_anova

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'f_reg_quality.csv')
df = pd.read_csv(data_path)

model="similarity ~ C(session) + C(subject)"
regression_model = smf.ols(model, data=df).fit()
anova_summary = sm.stats.anova_lm(regression_model, typ=2)

print(inline_anova(anova_summary,"C(subject)",style="tex", condensed=True, max_len=2, pythontex_safe=True))
