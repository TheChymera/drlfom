import samri.plotting.maps as maps

stat_map = "data/dw_l2_seed_joint/ses-ofM/acq-EPI_run-0_tstat.nii.gz"
template = "/usr/share/mouse-brain-templates/dsurqec_40micron_masked.nii"

maps.stat3D(stat_map,
	cut_coords=(0.0,-4.6,-3.4),
	scale=0.3,
	template=template,
	show_plot=False,
	threshold=4,
	threshold_mesh=4,
	)
