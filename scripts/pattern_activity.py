import pandas as pd
import matplotlib.pyplot as plt
import samri.plotting.maps as maps
from behaviopy.plotting import qualitative_times
from os import path

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'pattern_activity.csv')
df = pd.read_csv(data_path)

df = df.rename(columns={'t':'Arbitrary Units'})

# definitions for the axes
left, width = 0.06, 0.9
bottom, height = 0.06, 0.9

session_coordinates = [left, bottom, width, height]
roi_coordinates = [left+0.34, bottom+0.66, 0.36, 0.24]

fig = plt.figure(1)

ax1 = plt.axes(session_coordinates)
qualitative_times(df,
        ax=ax1,
        x='Session',
        y='Arbitrary Units',
        condition='treatment',
        unit='subject',
        order=['naïve','acute','chronic/2w','chronic/4w','post'],
        bp_style=False,
        palette=["#56B4E9", "#E09000"],
        renames={
                'Session':{
                        'ofM':'naïve',
                        'ofMaF':'acute',
                        'ofMcF1':'chronic/2w',
                        'ofMcF2':'chronic/4w',
                        'ofMpF':'post',
                        },
                },
        )

ax2 = plt.axes(roi_coordinates)
stat_map = path.abspath(path.expanduser('~/ni_data/ofM.dr/l2/best_responders/sessionofM/tstat1.nii.gz'))
template = path.abspath(path.expanduser('~/ni_data/templates/DSURQEc_40micron_masked.nii.gz'))
maps.stat(stat_maps=[stat_map],
        template=template,
        cut_coords=[(0,-4.3,-3.3)],
        annotate=False,
        scale=0.5,
        show_plot=False,
        interpolation=None,
        threshold=4,
        draw_colorbar=False,
        ax=ax2,
        )

