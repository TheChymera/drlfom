import samri.plotting.maps as maps
from matplotlib.colors import LinearSegmentedColormap

#colors = [
#	'#888888',
#	'#888888',
#	'#888888',
#	'#cc5a71',
#	'#cc5a71',
#	'#66d151', #green
#	'#66d151', #green
#	'#8900d8', #purple
#	'#8900d8', #purple
#	]
#colors = [
#	'#888888',
#	'#888888',
#	'#888888',
#	'#8b5fbf',
#	'#8b5fbf',
#	'#5dfdcb', #green
#	'#5dfdcb', #green
#	'#ff6978', #purple
#	'#ff6978', #purple
#	]
colors = [
	'#888888',
	'#888888',
	'#888888',
	'#571c82', #subctx
	'#571c82', #subctx
	'#daff38', #stem
	'#daff38', #stem
	'#ff49b3', #ctx
	'#ff49b3', #ctx
	]
n_bins = 9
cmap_name = "My Assignments Keymap"

cm = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)

stat_map = "data/dw_l2/assignment_4_spherical.nii.gz"
template = "/usr/share/mouse-brain-templates/dsurqec_40micron_masked.nii"

ax = maps.slices(stat_map,
	ratio=7/5.,
        auto_figsize=False,
        style=False,
	position_vspace=0.1,
        slice_spacing=0.45,
	#scale=0.2,
	#heatmap_threshold=0.9,
	heatmap_threshold=1.0,
	#cmap='tab10_r',
	cmap=cm,
	heatmap_alpha=0.8,
	positive_only=True,
	#display_mode=['y','x'],
	#shape='portrait',
	#draw_colorbar=False,
	#annotate=False,
	#bypass_cmap=True,
	skip_start=4,
	skip_end=2,
	)
