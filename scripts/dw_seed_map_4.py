import samri.plotting.maps as maps
from samri.fetch.local import roi_from_atlaslabel

stat_map = "data/dw_l2_seed/assignment_5_spherical.nii.gz"
template = "/usr/share/mouse-brain-templates/dsurqec_40micron_masked.nii"

myroi = roi_from_atlaslabel(stat_map,[4],
	save_as='/tmp/dw_seed_assignment_4.nii.gz',
	)

maps.stat3D('/tmp/dw_seed_assignment_4.nii.gz',
	scale=0.3,
	template=template,
	show_plot=False,
	threshold=0.8,
	threshold_mesh=0.8,
	draw_colorbar=False,
	cmap=['#E31A1C'],
	)
