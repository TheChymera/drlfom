import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.patheffects as path_effects
from samri.plotting.aggregate import roi_sums
from samri.report.roi import atlasassignment

img = "data/dw_l2/assignment_4_spherical.nii.gz"
df = atlasassignment(img,
	lateralized=False,
	value_label='Assignment',
	)

df['Structure'] = df['Structure'].replace(
	{
		'Dorsal nucleus of the endopiriform': 'Dorsal nucleus of the endopiriform claustrum',
		}
	)
df['Structure'] = df['Structure'].str.title()
roi_sums(df,
	ascending=False,
	max_rois=10,
	exclude_tissue_type=['CSF'],
	value_label='Assignment',
	roi_value=2,
	text_side='left',
	y_align=0.29,
	x_align=0.015,
	palette=['#571c82'],
	contour_ratio=0.1,
	text_color='#FFFFFF',
	)
