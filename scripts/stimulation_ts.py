from samri.fetch.local import roi_from_atlaslabel
from samri.utilities import bids_substitution_iterator
from samri.plotting import summary, timeseries

substitutions = bids_substitution_iterator(["ofM"],["5694"],["CogB"],'~/ni_data/ofM.dr/',"composite",['EPI'])
roi = roi_from_atlaslabel("/usr/share/mouse-brain-templates/dsurqec_40micron_labels.nii",
        mapping="/usr/share/mouse-brain-templates/dsurqe_labels.csv",
        label_names=["cortex"],
        )
timecourses, designs, stat_maps, events_dfs, subplot_titles = summary.ts_overviews(substitutions, roi,
        ts_file_template="~/ni_data/ofM.dr/preprocessing/{preprocessing_dir}/sub-{subject}/ses-{session}/func/sub-{subject}_ses-{session}_acq-{acquisition}_trial-{trial}_cbv.nii.gz",
        betas_file_template="~/ni_data/ofM.dr/l1/{l1_dir}/sub-{subject}/ses-{session}/sub-{subject}_ses-{session}_acq-{acquisition}_trial-{trial}_cbv_cope.nii.gz",
        design_file_template="~/ni_data/ofM.dr/l1/{l1_workdir}/_bids_dictionary_trial{trial}.session{session}.path..home..chymera..ni_data..ofM.dr..preprocessing..composite..sub-{subject}..ses-{session}..func..sub-{subject}_ses-{session}_acq-{acquisition}_trial-{trial}_cbv.nii.gz.modalitycbv.acquisition{acquisition}.subject{subject}/modelgen/run0.mat",
        event_file_template="~/ni_data/ofM.dr/preprocessing/{preprocessing_dir}/sub-{subject}/ses-{session}/func/sub-{subject}_ses-{session}_acq-{acquisition}_trial-{trial}_events.tsv",
        )
timeseries.multi(timecourses, designs, stat_maps, events_dfs, subplot_titles,
                figure="timecourses",
                colors=["#E44411","#4AA5E0","#8eccf0"],
                samri_style=False,
                )
