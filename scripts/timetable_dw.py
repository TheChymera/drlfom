import pandas as pd
from behaviopy import plotting

df = pd.read_csv('../data/timetable_dw.csv')

shade = ["FMRIMeasurement_date"]
draw = [
        {"TreatmentProtocol_code":["aFluIV_","Treatment_start_date"]},
        {"TreatmentProtocol_code":["aFluIV","Treatment_start_date"]},
        "OpenFieldTestMeasurement_date",
        "ForcedSwimTestMeasurement_date",
        {"TreatmentProtocol_code":["aFluSC","Treatment_start_date"]},
        ]
saturate = [
        {"Cage_TreatmentProtocol_code":["cFluDW","Cage_Treatment_start_date","Cage_Treatment_end_date"]},
        {"Cage_TreatmentProtocol_code":["cFluDW_","Cage_Treatment_start_date","Cage_Treatment_end_date"]},
        {"TreatmentProtocol_code":["cFluIP","Treatment_start_date","Treatment_end_date"]},
        ]

plotting.timetable(df, "subject",
        draw=draw,
        shade=shade,
        saturate=saturate,
        draw_colors=["#56B4E9", "#E09000", "#009E73", "#000000","#F0E442", "#0072B2", "#D55E00", "#CC79A7"],
        integer_dates=True,
        bp_style=False,
        )
