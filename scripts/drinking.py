from labbookdb.report.tracking import treatment_group, append_external_identifiers, qualitative_dates, cage_consumption
from labbookdb.report.selection import cage_periods, cage_drinking_measurements
from behaviopy.plotting import qualitative_times

db_path = '~/syncdata/meta.db'
df = cage_drinking_measurements(db_path, ['cFluDW','cFluDW_'])
df = cage_consumption(db_path, df)


fuzzy_matching = {
                "naïve":[-14,-15,-13,-7,-8,-6],
                "acute":[0,-1],
                "chronic/2w":[14,13,15],
                "chronic/4w":[28,27,29],
                "post":[45,44,46],
        }
df = qualitative_dates(df,
	iterator_column='Cage_id',
	date_column='relative_end_date',
	label='qualitative_date',
	fuzzy_matching=fuzzy_matching,
	)
df.rename(index=str, columns={"qualitative_date": "Session", "day_animal_consumption": "Daily Intake [ml]"}, inplace=True)
qualitative_times(df,
	x='Session',
	y='Daily Intake [ml]',
	unit='Cage_id',
	order=['naïve','acute','chronic/2w','chronic/4w','post'],
	condition='TreatmentProtocol_code',
        bp_style=False,
        palette=["#56B4E9", "#E09000"],
        renames={
                'TreatmentProtocol_code':{
                        'cFluDW':'Fluoxetine',
                        'cFluDW_':'Vehicle',
                        },
                }

	)
