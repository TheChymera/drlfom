from samri.plotting import summary
from samri.report import roi
import samri.plotting.maps as maps
from samri.fetch.local import roi_from_atlaslabel
import matplotlib.pyplot as plt
from os import path

stat_map = path.abspath(path.expanduser('~/ni_data/ofM.dr/l2/anova/anova_zfstat.nii.gz'))
template = path.abspath(path.expanduser('~/ni_data/templates/DSURQEc_40micron_masked.nii.gz'))
maps.stat(stat_maps=[stat_map],
        template=template,
        cut_coords=[None],
        annotate=True,
        scale=1,
        show_plot=False,
        interpolation=None,
        threshold=0,
        draw_colorbar=False,
        )
