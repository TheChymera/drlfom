import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import samri.plotting.maps as maps
import seaborn as sns
from os import path

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'dw_seed_4.csv')
df = pd.read_csv(data_path)
df = df.loc[~df['Exclude']]
df = df.loc[(df['Contrast']=='cbv')]

myroi = "data/dw_l2_seed/assignment_5_spherical.nii.gz"

# Style elements
palette=[
	"#E09000",
	"#56B4E9",
	]

df['Session']=df['Session'].map({
	'ofM':'naïve',
	'ofMaF':'acute',
	'ofMcF1':'chronic/2w',
	'ofMcF2':'chronic/4w',
	'ofMpF':'post',
	})

# definitions for the axes
left, width = mpl.rcParams["figure.subplot.left"], mpl.rcParams["figure.subplot.right"]
bottom, height = mpl.rcParams["figure.subplot.bottom"], mpl.rcParams["figure.subplot.top"]

session_coordinates = [left, bottom, width, height]
roi_coordinates = [left+0.57, bottom-0.02, 0.315, 0.21]

fig = plt.figure(1)
ax1 = plt.axes(session_coordinates)
sns.pointplot(
	x='Session',
	y='Mean ROI t',
	units='Subject',
	data=df,
	hue='Treatment',
	dodge=True,
	palette=palette,
	order=['naïve','acute','chronic/2w','chronic/4w','post'],
	ax=ax1,
	ci=95,
	)

ax2 = plt.axes(roi_coordinates)
maps.atlas_label(myroi,
	scale=0.5,
	color="#E31A1C",
	ax=ax2,
	annotate=False,
	label_names=[4],
	alpha=0.8,
	overlay_alpha=0.75,
	mapping=None,
	)

# Hack to display zero in axes
ax1.axhline(y=0, color='k', alpha=0.0)
