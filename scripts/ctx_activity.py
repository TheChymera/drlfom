import pandas as pd
import matplotlib.pyplot as plt
import samri.plotting.maps as maps
from os import path
import seaborn as sns

# Style elements
palette=["#56B4E9", "#E09000"]

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'ctx_activity.csv')
df = pd.read_csv(data_path)

df = df.rename(columns={'t':'Mean t-Statistic',})
df['Session']=df['Session'].map({
        'ofM':'naïve',
        'ofMaF':'acute',
        'ofMcF1':'chronic/2w',
        'ofMcF2':'chronic/4w',
        'ofMpF':'post',
        })


# definitions for the axes
left, width = 0.06, 0.9
bottom, height = 0, 0.9

session_coordinates = [left, bottom, width, height]
roi_coordinates = [left+0.53, bottom+0.01, 0.36, 0.24]

fig = plt.figure(1)
ax1 = plt.axes(session_coordinates)
sns.pointplot(
	x='Session',
	y='Mean t-Statistic',
	units='subject',
	data=df,
	hue='treatment',
	dodge=True,
	palette=palette,
	order=['naïve','acute','chronic/2w','chronic/4w','post'],
	ax=ax1,
	ci=95,
	)


ax1.xaxis.tick_top()
ax1.xaxis.set_label_position('top')

ax2 = plt.axes(roi_coordinates)
maps.atlas_label("/usr/share/mouse-brain-templates/dsurqec_40micron_labels.nii",
        scale=0.5,
        color="#E09000",
        ax=ax2,
        annotate=False,
        mapping="/usr/share/mouse-brain-templates/dsurqe_labels.csv",
        label_names=["cortex","Cortex"],
        alpha=0.8,
        )

# Hack to display zero in axes
ax1.axhline(y=0, color='k', alpha=0.0)
