import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import samri.plotting.maps as maps
import seaborn as sns
from os import path

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'iv_activity_ctx.csv')
df = pd.read_csv(data_path)
df = df.loc[~df['Exclude']]
df = df.loc[(df['Contrast']=='cbv')]

# Style elements
color="#E09000"

df['Session']=df['Session'].map({
	'ofM':'naïve',
	'ofMaF':'acute',
	'ofMcF1':'chronic/2w',
	'ofMcF2':'chronic/4w',
	'ofMpF':'post',
	})

# definitions for the axes
left, width = mpl.rcParams["figure.subplot.left"], mpl.rcParams["figure.subplot.right"]
bottom, height = mpl.rcParams["figure.subplot.bottom"], mpl.rcParams["figure.subplot.top"]

session_coordinates = [left, bottom, width, height]
roi_coordinates = [left+0.57, bottom-0.02, 0.315, 0.21]

fig = plt.figure(1)
ax1 = plt.axes(session_coordinates)
sns.pointplot(
	x='Session',
	y='Mean Ctx t',
	units='Subject',
	data=df,
	dodge=True,
	order=['naïve','acute','chronic/2w','chronic/4w','post'],
	color=color,
	ax=ax1,
	ci=95,
	)

ax2 = plt.axes(roi_coordinates)
maps.atlas_label("/usr/share/mouse-brain-templates/dsurqec_40micron_labels.nii",
	scale=0.5,
	color="#E09000",
	ax=ax2,
	annotate=False,
	mapping="/usr/share/mouse-brain-templates/dsurqe_labels.csv",
	label_names=["cortex","Cortex"],
	alpha=0.8,
	)

# Hack to display zero in axes
ax1.axhline(y=0, color='k', alpha=0.0)
