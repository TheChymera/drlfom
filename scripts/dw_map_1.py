import samri.plotting.maps as maps
from samri.fetch.local import roi_from_atlaslabel

stat_map = "data/dw_l2/assignment_4_spherical.nii.gz"
template = "/usr/share/mouse-brain-templates/dsurqec_40micron_masked.nii"

myroi = roi_from_atlaslabel(stat_map,[2],
	save_as='/tmp/dw_assignment_2.nii.gz',
	mapping=None,
	)

maps.stat3D('/tmp/dw_assignment_2.nii.gz',
	scale=0.3,
	alpha=0.75,
	template=template,
	show_plot=False,
	threshold=0.8,
	threshold_mesh=0.8,
	draw_colorbar=False,
	cmap=['#571c82'],
	)
