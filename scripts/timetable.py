import pandas as pd

from behaviopy import plotting
from labbookdb.report.tracking import overview
from labbookdb.report.selection import animal_id
from os import path

db_path = '~/syncdata/meta.db'
shade = ["FMRIMeasurement_date"]
draw = [
        {"TreatmentProtocol_code":["aFluIV_","Treatment_start_date"]},
        {"TreatmentProtocol_code":["aFluIV","Treatment_start_date"]},
        "OpenFieldTestMeasurement_date",
        "ForcedSwimTestMeasurement_date",
        {"TreatmentProtocol_code":["aFluSC","Treatment_start_date"]},
        ]
saturate = [
        {"Cage_TreatmentProtocol_code":["cFluDW","Cage_Treatment_start_date","Cage_Treatment_end_date"]},
        {"Cage_TreatmentProtocol_code":["cFluDW_","Cage_Treatment_start_date","Cage_Treatment_end_date"]},
        {"TreatmentProtocol_code":["cFluIP","Treatment_start_date","Treatment_end_date"]},
        ]

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'ctx_activity.csv')
data_example = pd.read_csv(data_path)
subjects = list(data_example['subject'].unique())
subject_ids = []
for subject in subjects:
        subject_id = animal_id(db_path, 'ETH/AIC', str(subject))
        subject_ids.append(subject_id)

myfilter = ["Animal","id",]
myfilter.extend(subject_ids)
filters = [myfilter]
filters.append(["Cage_Treatment","start_date","2016,11,24,21,30","2017,1,31,22,0","2016,5,19,23,5","2017,5,26,17,0"])

df = overview(db_path,
        filters=filters,
        default_join="outer",
        join_types=["outer","outer","outer","outer","outer","outer","outer","outer","outer","outer"],
        )
plotting.timetable(df, "Animal_id",
        draw=draw,
        shade=shade,
        saturate=saturate,
        draw_colors=["#56B4E9", "#E09000", "#009E73", "#000000","#F0E442", "#0072B2", "#D55E00", "#CC79A7"],
        integer_dates=True,
        bp_style=False,
        window_end=49,
        )
