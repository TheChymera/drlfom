import matplotlib.pyplot as plt
import pandas as pd
import samri.plotting.maps as maps
from behaviopy.plotting import qualitative_times
from os import path

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'ctx_drs_sfc.csv')
df = pd.read_csv(data_path)

df = df.rename(columns = {'z':'Mean z-Statistic'})

# definitions for the axes
left, width = 0.06, 0.87
bottom, height = 0, 0.9

session_coordinates = [left, bottom, width, height]
roi_coordinates = [left+0.5, bottom+0.01, 0.36, 0.24]

fig = plt.figure(1)

ax1 = plt.axes(session_coordinates)
qualitative_times(df,
        ax=ax1,
        x='Session',
        y='Mean z-Statistic',
        condition='treatment',
        unit='subject',
        order=['naïve','acute','chronic/2w','chronic/4w','post'],
        bp_style=False,
        palette=["#56B4E9", "#E09000"],
        renames={
                'Session':{
                        'ofM':'naïve',
                        'ofMaF':'acute',
                        'ofMcF1':'chronic/2w',
                        'ofMcF2':'chronic/4w',
                        'ofMpF':'post',
                        },
                },
        )
ax1.xaxis.tick_top()
ax1.xaxis.set_label_position('top')

ax2 = plt.axes(roi_coordinates)
maps.atlas_label("~/ni_data/templates/roi/DSURQEc_200micron_labels.nii",
        scale=0.5,
        color="#E09000",
        ax=ax2,
        annotate=False,
        mapping="~/ni_data/templates/roi/DSURQE_mapping.csv",
        label_names=["cortex"],
        alpha=0.8,
        )

# Hack to display zero in axes
ax1.axhline(y=0, color='k', alpha=0.0)
