import pandas as pd
from labbookdb.report.selection import animal_id
from os import path
from samri.plotting.aggregate import registration_qc

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'f_reg_quality.csv')
df = pd.read_csv(data_path)

#for subject in df['subject']:
#	if len(df[df['subject']==subject]) <= 2:
#		df = df.drop(df[df['subject']==subject].index)

for ix, row in df.iterrows():
	new_id = animal_id('~/syncdata/meta.db','ETH/AIC',str(row['subject']))
	df.set_value(ix,'subject',new_id)
#df['subject'] = df['subject'].apply(lambda x: animal_id('~/syncdata/meta.db','ETH/AIC',str(x)))

registration_qc(df,
	cmap='tab10',
	value={"similarity":"Similarity"},
	group={"subject":"Subject"},
	repeat={"session":"Session"},
	show=False,
	samri_style=False,
	values_rename={
		"ofM":"naïve",
		'ofMaF':'acute',
		'ofMcF1':'chronic/2w',
		'ofMcF2':'chronic/4w',
		'ofMpF':'post',
		},
	)
