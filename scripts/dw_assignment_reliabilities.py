import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import samri.plotting.maps as maps
import seaborn as sns
from os import path

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'dw_assignment_reliabilities.csv')
df = pd.read_csv(data_path)

df['Covariance'] = df['Covariance'].replace('diag','diagonal') 

sns.pointplot(
	x='Components',
	y='Reliability [%]',
	data=df,
	hue='Covariance',
	dodge=.25,
	palette='Set2',
	ci=95,
	)
