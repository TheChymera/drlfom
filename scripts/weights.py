import datetime as dt
from labbookdb.report.tracking import animal_weights, qualitative_dates
from behaviopy.plotting import qualitative_times

fuzzy_matching = {
		"naïve":[-14,-15,-13,-7,-8,-6],
		"acute":[0,-1],
		"chronic/2w":[14,13,15],
		"chronic/4w":[28,27,29],
		"post":[45,44,46],
	}

df = animal_weights('~/syncdata/meta.db', {'cage':['cFluDW','cFluDW_']})
df = df[df['Cage_Treatment_start_date']>=dt.datetime(2017, 1, 31)]
df['relative_date'] = df['relative_date'].dt.days.astype(int)
df = df[['Animal_id', 'relative_date', 'weight', 'Cage_TreatmentProtocol_code', 'ETH/AIC']]
df = qualitative_dates(df, fuzzy_matching=fuzzy_matching)
df.rename(index=str, columns={"qualitative_date": "Session", "weight": "Weight [g]"}, inplace=True)
qualitative_times(df,
	x='Session',
	y='Weight [g]',
	condition='Cage_TreatmentProtocol_code',
	err_style="boot_traces",
	order=['naïve','acute','chronic/2w','chronic/4w','post'],
	bp_style=False,
	palette=["#56B4E9", "#E09000"],
	renames={
		'Cage_TreatmentProtocol_code':{
			'cFluDW':'Fluoxetine',
			'cFluDW_':'Vehicle',
			},
		}
	)
