import samri.plotting.maps as maps
from matplotlib.colors import LinearSegmentedColormap, ListedColormap

#colors = [
#	'#888888',
#	'#888888',
#	'#888888',
#	'#571c82', #subctx
#	'#571c82', #subctx
#	'#daff38', #stem
#	'#daff38', #stem
#	'#1affff', #other stem
#	'#1affff', #other stem
#	'#ff49b3', #ctx
#	'#ff49b3', #ctx
#	]
#colors = [
#	'#888888',
#	'#888888',
#	'#888888',
#	'#571c82', #subctx
#	'#571c82', #subctx
#	'#ff4040', #x
#	'#ff4040', #x
#	'#daff38', #stem
#	'#daff38', #stem
#	'#1affff', #other stem
#	'#1affff', #other stem
#	'#ff49b3', #ctx
#	'#ff49b3', #ctx
#	]
	#'#999999',
#n_bins = 13
colors = [
	'0.4',
	'#571c82', #subctx
	'#ff4040', #x
	'#daff38', #stem
	'#1affff', #other stem
	'#ff49b3', #ctx
	]
n_bins = 6
cmap_name = "My Assignments Keymap"

#cm = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins).resampled(6)
cm = ListedColormap(colors, N=n_bins)

stat_map = "data/dw_l2/assignment_5_spherical.nii.gz"
template = "/usr/share/mouse-brain-templates/dsurqec_40micron_masked.nii"

ax = maps.slices(stat_map,
	heatmap_threshold=0.1,
	ratio=7/5.,
        auto_figsize=False,
        style=False,
	position_vspace=0.1,
        slice_spacing=0.45,
	#scale=0.2,
	#heatmap_threshold=0.9,
	#cmap='tab10_r',
	cmap=cm,
	heatmap_alpha=0.8,
	positive_only=True,
	#display_mode=['y','x'],
	#shape='portrait',
	#draw_colorbar=False,
	#annotate=False,
	#bypass_cmap=True,
	skip_start=3,
	skip_end=3,
	no_ticks=True,
	)
#ax = maps.stat([stat_map,stat_map],
#	template=template,
#	scale=0.2,
#	threshold=0,
#	cmap='Dark2_r',
#	alpha=0.75,
#	positive_only=False,
#	display_mode=['y','x'],
#	shape='portrait',
#	draw_colorbar=False,
#	annotate=False,
#	bypass_cmap=True,
#	)
