import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import samri.plotting.maps as maps
import seaborn as sns
from os import path

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'dw_seed_assignment_reliabilities.csv')
df = pd.read_csv(data_path)

sns.pointplot(
	x='Components',
	y='Reliability [%]',
	#units='Subject',
	data=df,
	hue='Covariance',
	dodge=.25,
	#palette=palette,
	#ax=ax1,
	ci=95,
	)
