import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.formula.api as smf
from os import path
from lib.utils import float_to_tex, inline_anova, inline_factor
from samri.report.roi import atlasassignment

def events_tab(events_path):
	df = pd.read_csv(events_path,sep='\t')
	df = df[['onset','duration','frequency','pulse_width']]
	df = df.rename(columns={
		'onset'	 :   '\makecell[r]{Onset \\\\ {[}s{]}}',
		'duration'      :   '\makecell[r]{Duration \\\\ {[}s{]}}',
		'frequency'     :   '\makecell[r]{Frequency \\\\ {[}Hz{]}}',
		'pulse_width'   :   '\makecell[r]{Pulse Width \\\\ {[}s{]}}',
		'wavelength'    :   '\makecell[r]{Wavelength \\\\ {[}nm{]}}',
		})
	df = df.reset_index(drop=True)
	df_tex = df.to_latex(index=False, escape=False)
	return df_tex

def subject_counter(
	data_path='data/dw_activity_dr.csv',
	criteria={'Treatment':['Fluoxetine'],}
	):
	df = pd.read_csv(data_path)
	for key in criteria:
		df = df.loc[df[key].isin(criteria[key])]
	subjects = df['Subject'].unique()
	count = len(subjects)
	return count

def lateral_session(data_path,
	directories=[''],
	sessions=[],
	factor='Side',
	typ=3,
	**kwargs
	):
	value_label='t Values'
	dfs=[]
	for d in directories:
		for i in sessions:
			data_path_i = data_path.format(d,i)
			df_ = atlasassignment(data_path_i,
				lateralized=True,
				value_label=value_label,
				)
			df_[value_label] = df_[value_label].apply(lambda x: np.mean([float(i) for i in x.split(', ')]))
			df_['Session'] = i
			dfs.append(df_)
	df = pd.concat(dfs)
	df = df.loc[df['Side'].isin(['left','right'])]
	dependent_variable = 'Q("{}")'.format(value_label)
	expression = 'Session*Side'
	formula = '{} ~ {}'.format(dependent_variable, expression)
	ols = smf.ols(formula, df).fit()
	summary = sm.stats.anova_lm(ols, typ=typ, robust='hc3')
	tex = inline_anova(summary, factor, 'tex', **kwargs)
	return tex

def lateral(data_path,
	):
	from scipy.stats import ttest_rel
	value_label='t Values'
	df = atlasassignment(data_path,
		lateralized=True,
		value_label=value_label,
		)
	df[value_label] = df[value_label].apply(lambda x: np.mean([float(i) for i in x.split(', ')]))
	p = ttest_rel(df.loc[df['Side']=='left',value_label], df.loc[df['Side']=='right',value_label]).pvalue
	p = float_to_tex(p)
	return p

def posthoc_t(comparison,
	data_path='data/dw_activity_dr.csv',
	dependent_variable='Q("Mean DR t")',
	expression='Treatment:C(Session)+Session-1',
	exclusion_criteria={},
	max_len=2,
	all_comparisons=[
		'Treatment[Fluoxetine]:C(Session)[1] = Treatment[Vehicle]:C(Session)[1]',
		'Treatment[Fluoxetine]:C(Session)[2] = Treatment[Vehicle]:C(Session)[2]',
		'Treatment[Fluoxetine]:C(Session)[3] = Treatment[Vehicle]:C(Session)[3]',
		'Treatment[Fluoxetine]:C(Session)[4] = Treatment[Vehicle]:C(Session)[4]',
		'Treatment[Fluoxetine]:C(Session)[5] = Treatment[Vehicle]:C(Session)[5]',
		],
	**kwargs
	):
	from statsmodels.stats.multitest import multipletests
	df = pd.read_csv(path.abspath(data_path))

	for key in exclusion_criteria.keys():
		df = df.loc[~df[key].isin(exclusion_criteria[key])]

	df = df.loc[~df['Exclude']]
	df = df.loc[(df['Contrast']=='cbv')]
	di = {
		'ofM':1,
		'ofMaF':2,
		'ofMcF1':3,
		'ofMcF2':4,
		'ofMpF':5,
		}
	df["Session"].replace(di, inplace=True)

	formula = '{} ~ {}'.format(dependent_variable, expression)
	model = smf.ols(formula, df)
	fit = model.fit()
	summary = fit.summary()
	t = fit.t_test(all_comparisons)
	g, corrected_pvalues, _, _ = multipletests(t.pvalue, alpha=0.05, method='fdr_bh')
	p = corrected_pvalues[all_comparisons.index(comparison)]
	p = float_to_tex(p, max_len=max_len, **kwargs)
	return p

def factorci(factor,
	data_path='data/dw_activity_dr.csv',
	dependent_variable='Q("Mean DR t")',
	expression='Treatment:C(Session)+Session',
	exclusion_criteria={},
	**kwargs
	):
	df = pd.read_csv(path.abspath(data_path))

	for key in exclusion_criteria.keys():
		df = df.loc[~df[key].isin(exclusion_criteria[key])]

	df = df.loc[~df['Exclude']]
	df = df.loc[(df['Contrast']=='cbv')]

	di = {
		'ofM':1,
		'ofMaF':2,
		'ofMcF1':3,
		'ofMcF2':4,
		'ofMpF':5,
		}
	df["Session"].replace(di, inplace=True)

	formula = '{} ~ {}'.format(dependent_variable, expression)
	model = smf.ols(formula, df)
	fit = model.fit()
	summary = fit.summary()
	tex = inline_factor(summary, factor, 'tex', **kwargs)
	return tex

def anova(
	data_path='data/dw_activity_dr.csv',
	dependent_variable='Q("Mean DR t")',
	expression='Session+Treatment:C(Session)',
	factor='Treatment:C(Session)',
	typ=3,
	**kwargs
	):
	df = pd.read_csv(path.abspath(data_path))

	df = df.loc[~df['Exclude']]
	df = df.loc[(df['Contrast']=='cbv')]

	di = {
		'ofM':1,
		'ofMaF':2,
		'ofMcF1':3,
		'ofMcF2':4,
		'ofMpF':5,
		}
	df["Session"].replace(di, inplace=True)

	formula = '{} ~ {}'.format(dependent_variable, expression)
	model = smf.ols(formula, df)
	fit = model.fit()
	summary = sm.stats.anova_lm(fit, typ=typ, robust='hc3')
	tex = inline_anova(summary, factor, 'tex', **kwargs)
	return tex

##########################
def fstatistic(factor,
	df_path='data/volumes.csv',
	dependent_variable='Volume Change Factor',
	expression='Processing*Template',
	exclusion_criteria={},
	**kwargs
	):
	df_path = path.abspath(df_path)
	df = pd.read_csv(df_path)

	df = df.loc[df['Processing']!='Unprocessed']

	for key in exclusion_criteria.keys():
		df = df.loc[~df[key].isin(exclusion_criteria[key])]

	formula='Q("{}") ~ {}'.format(dependent_variable, expression)
	ols = smf.ols(formula, df).fit()
	anova = sm.stats.anova_lm(ols, typ=2)
	tex = inline_anova(anova, factor, 'tex', **kwargs)
	return tex

def corecomparison_factorci(factor,
	df_path='data/volumes.csv',
	dependent_variable='Volume Change Factor',
	expression='Processing*Contrast',
	exclusion_criteria={},
	**kwargs
	):
	df_path = path.abspath(df_path)
	df = pd.read_csv(df_path)

	df = df.loc[df['Processing']!='Unprocessed']
	df = df.loc[((df['Processing']=='Legacy') & (df['Template']=='Legacy')) | ((df['Processing']=='Generic') & (df['Template']=='Generic'))]

	for key in exclusion_criteria.keys():
		df = df.loc[~df[key].isin(exclusion_criteria[key])]

	formula = 'Q("{}") ~ {}'.format(dependent_variable, expression)
	model = smf.mixedlm(formula, df, groups='Uid')
	fit = model.fit()
	summary = fit.summary()
	tex = inline_factor(summary, factor, 'tex', **kwargs)
	return tex

def vcc_factorci(factor,
	df_path='data/volumes.csv',
	**kwargs
	):
	df_path = path.abspath(df_path)
	df = pd.read_csv(df_path)

	df = df.loc[df['Processing']!='Unprocessed']
	df = df.loc[((df['Processing']=='Legacy') & (df['Template']=='Legacy')) | ((df['Processing']=='Generic') & (df['Template']=='Generic'))]

	model=smf.mixedlm('Q("Volume Change Factor") ~ Processing*Contrast', df, groups='Uid')

	fit = model.fit()
	summary = fit.summary()
	tex = inline_factor(summary, factor, 'tex', **kwargs)
	return tex

def varianceratio(
	df_path='data/volumes.csv',
	template=False,
	dependent_variable='Volume Change Factor',
	max_len=2,
	**kwargs
	):

	df_path = path.abspath(df_path)
	df = pd.read_csv(df_path)

	df = df.loc[df['Processing']!='Unprocessed']

	if template:
		df = df.loc[df['Template']==template]
	legacy = np.var(df.loc[df['Processing']=='Legacy', dependent_variable].tolist())
	generic = np.var(df.loc[df['Processing']=='Generic', dependent_variable].tolist())


	ratio = legacy/generic

	return float_to_tex(ratio, max_len, **kwargs)
	# Hypothesis test, but we are not, in current cases, testing a hypothesis.
	#from scipy.stats import levene
	#result = levene(
	#	df.loc[df['Processing']=='Legacy', 'Volume Change Factor'].tolist(),
	#	df.loc[df['Processing']=='Generic', 'Volume Change Factor'].tolist(),
	#	)
	#print(float_to_tex(result.pvalue, max_len=3))

def variancep(
	df_path='data/volumes.csv',
	template=False,
	max_len=2,
	**kwargs
	):
	from scipy.stats import levene

	volume_path = path.abspath('data/volumes.csv')
	df = pd.read_csv(volume_path)

	df = df.loc[df['Processing']!='Unprocessed']

	if template:
		df = df.loc[df['Template']==template]
	result = levene(
		df.loc[df['Processing']=='Legacy', 'Volume Change Factor'].tolist(),
		df.loc[df['Processing']=='Generic', 'Volume Change Factor'].tolist(),
		)

	return float_to_tex(result.pvalue, max_len, **kwargs)

def variance_test(
	factor,
	workflow,
	metric,
	df_path='data/variance_data.csv',
	template=False,
	max_len=2,
	**kwargs
	):
	df = pd.read_csv(path.abspath(df_path))
	df = df.loc[df['Processing']==workflow]
	#contrast
	df = df.loc[df['acquisition'].str.contains('cbv')]
	model= metric + '~ C(Subject) + C(Session)'
	ols = smf.ols(model, df).fit()
	anova = sm.stats.anova_lm(ols, typ=3, robust='hc3')
	tex = inline_anova(anova, factor, 'tex', **kwargs)
	return tex

