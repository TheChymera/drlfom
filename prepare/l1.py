from os import path
from samri.pipelines import glm

scratch_dir = '~/.scratch/drlfom'

##DW
### No BOLD modelling due to insufficient session coverage
preprocess_base = '{}/dw_preprocessing/'.format(scratch_dir)
glm.l1(preprocess_base,
	bf_path='../data/chr_beta1.txt',
	workflow_name='dw_l1',
	habituation="confound",
	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	keep_work=False,
	n_jobs_percentage=.4,
	highpass_sigma=225,
	match={
		'acquisition':['EPI'],
		'run':['0'],
		},
	invert=True,
	out_base=scratch_dir,
	)
glm.seed(preprocess_base,'/usr/share/mouse-brain-templates/dsurqec_200micron_roi-dr.nii',
	mask='mouse',
	n_jobs_percentage=.33,
	match={
		'acquisition':['EPI'],
		'run':['0'],
		},
	lowpass_sigma=2,
	highpass_sigma=225,
	out_base=scratch_dir,
	workflow_name='dw_l1_seed',
	metric='median',
	top_voxel='{}/dw_l1/sub-{{subject}}/ses-{{session}}/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-{{acquisition}}_run-{{run}}_{{suffix}}_tstat.nii.gz'.format(scratch_dir),
	)

##IV
preprocess_base = '{}/iv_preprocessing/'.format(scratch_dir)
## CBV
glm.l1(preprocess_base,
	bf_path='../data/chr_beta1.txt',
	workflow_name='iv_l1',
	habituation="confound",
	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	keep_work=False,
	n_jobs_percentage=.4,
	highpass_sigma=225,
	match={
		'acquisition':['EPIlowcov'],
		'run':['1'],
		},
	invert=True,
	out_base=scratch_dir,
	)
# BOLD
glm.l1(preprocess_base,
	bf_path='../data/chr_beta1.txt',
	workflow_name='iv_l1',
	habituation="confound",
	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	keep_work=False,
	n_jobs_percentage=.4,
	highpass_sigma=225,
	match={
		'acquisition':['EPIlowcov'],
		'run':['0'],
		},
	out_base=scratch_dir,
	)
glm.seed(preprocess_base,'/usr/share/mouse-brain-templates/dsurqec_200micron_roi-dr.nii',
	mask='mouse',
	n_jobs_percentage=.33,
	match={
		'acquisition':['EPIlowcov'],
		},
	lowpass_sigma=2,
	highpass_sigma=225,
	out_base=scratch_dir,
	workflow_name='iv_l1_seed',
	metric='median',
	top_voxel='{}/iv_l1/sub-{{subject}}/ses-{{session}}/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-{{acquisition}}_run-{{run}}_{{suffix}}_tstat.nii.gz'.format(scratch_dir),
	)
