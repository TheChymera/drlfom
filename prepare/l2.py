import pandas as pd
from os import path
from samri.pipelines import glm

scratch_dir = '~/.scratch/drlfom'
data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'dw_activity_dr.csv')
df = pd.read_csv(data_path)
df = df.loc[~df['Exclude']]

sessions = ['ofM','ofMaF','ofMcF1','ofMcF2','ofMpF']

# DW Data --- need to split by treatment group
for ses in sessions:
	#Fluoxetine
	sub = df.loc[(df['Session']==ses)&(df['Treatment']=='Fluoxetine'),'Subject'].to_list()
	l1_base = '{}/dw_l1/'.format(scratch_dir)
	glm.l2_common_effect(l1_base,
		workflow_name='dw_l2',
		mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
		groupby='session',
		keep_work=False,
		match={
			'subject':[str(i) for i in sub],
			'session':[ses]
			},
		n_jobs_percentage=.33,
		out_base=scratch_dir,
		)
	l1_base = '{}/dw_l1_seed/'.format(scratch_dir)
	glm.l2_common_effect(l1_base,
		workflow_name='dw_l2_seed',
		mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
		groupby='session',
		keep_work=False,
		match={
			'subject':[str(i) for i in sub],
			'session':[ses]
			},
		n_jobs_percentage=.33,
		out_base=scratch_dir,
		)
	#Vehicle
	sub = df.loc[(df['Session']==ses)&(df['Treatment']=='Vehicle'),'Subject'].to_list()
	l1_base = '{}/dw_l1/'.format(scratch_dir)
	glm.l2_common_effect(l1_base,
		workflow_name='dw_l2_',
		mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
		groupby='session',
		keep_work=False,
		match={
			'subject':[str(i) for i in sub],
			'session':[ses]
			},
		n_jobs_percentage=.33,
		out_base=scratch_dir,
		)
	# unneeded atm
	#l1_base = '{}/dw_l1_seed/'.format(scratch_dir)
	#glm.l2_common_effect(l1_base,
	#	workflow_name='dw_l2_seed_',
	#	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	#	groupby='session',
	#	keep_work=False,
	#	match={
	#		'subject':[str(i) for i in sub],
	#		'session':[ses]
	#		},
	#	n_jobs_percentage=.33,
	#	out_base=scratch_dir,
	#	)
	# No BOLD modelling due to insufficient session coverage
# Joint Fluoxetine and control baseline
l1_base = '{}/dw_l1/'.format(scratch_dir)
glm.l2_common_effect(l1_base,
	workflow_name='dw_l2_joint',
	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	groupby='session',
	keep_work=False,
	match={
		'session':['ofM']
		},
	n_jobs_percentage=.33,
	out_base=scratch_dir,
	)
l1_base = '{}/dw_l1_seed/'.format(scratch_dir)
glm.l2_common_effect(l1_base,
	workflow_name='dw_l2_seed_joint',
	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	groupby='session',
	keep_work=False,
	match={
		'session':['ofM']
		},
	n_jobs_percentage=.33,
	out_base=scratch_dir,
	)

# contrast between vehicle and fluoxetine groups in post-treatment session
fluoxetine_sub = df.loc[(df['Treatment']=='Fluoxetine'),'Subject'].to_list()
vehicle_sub = df.loc[(df['Treatment']=='Vehicle'),'Subject'].to_list()
vehicle_sub = [str(i) for i in vehicle_sub]
fluoxetine_sub = [str(i) for i in fluoxetine_sub]
glm.l2_controlled_effect(l1_base,
	workflow_name='dw_l2_post',
	#out_dir='{}/l2Manual/alias-block_other_controlled'.format(scratch_dir),
	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	n_jobs_percentage=.33,
	exclude={'session':[
		'ofM',
		'ofMaF',
		'ofMcF1',
		'ofMcF2',
		],},
	out_base=scratch_dir,
	match={'subject':fluoxetine_sub},
	control_match={'subject':vehicle_sub},
	run_mode='fe',
	)

# IV Data --- has no controls
# unneeded atm
#l1_base = '{}/iv_l1/'.format(scratch_dir)
#glm.l2_common_effect(l1_base,
#	workflow_name='iv_l2',
#	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
#	groupby='session',
#	keep_work=False,
#	match={
#		'run':['1'],
#		'acquisition':['EPIlowcov']
#		},
#	n_jobs_percentage=.33,
#	out_base=scratch_dir,
#	)
#l1_base = '{}/iv_l1_seed/'.format(scratch_dir)
#glm.l2_common_effect(l1_base,
#	workflow_name='iv_l2_seed',
#	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
#	groupby='session',
#	keep_work=False,
#	match={
#		'run':['1'],
#		'acquisition':['EPIlowcov']
#		},
#	n_jobs_percentage=.33,
#	out_base=scratch_dir,
#	)
#
##IV
#l1_base = '{}/iv_l1/'.format(scratch_dir)
#glm.l2_common_effect(l1_base,
#	workflow_name='iv_l2',
#	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
#	groupby='session',
#	keep_work=False,
#	match={
#		'run':['0'],
#		'acquisition':['EPIlowcov']
#		},
#	n_jobs_percentage=.33,
#	out_base=scratch_dir,
#	)
#l1_base = '{}/iv_l1_seed/'.format(scratch_dir)
#glm.l2_common_effect(l1_base,
#	workflow_name='iv_l2_seed',
#	mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
#	groupby='session',
#	keep_work=False,
#	match={
#		'run':['0'],
#		'acquisition':['EPIlowcov']
#		},
#	n_jobs_percentage=.33,
#	out_base=scratch_dir,
#	)
