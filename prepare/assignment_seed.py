import pandas as pd

from copy import deepcopy

from samri.report.snr import df_roi_data, df_significant_signal
from samri.utilities import bids_autofind_df, session_irregularity_filter
from samri.fetch.local import roi_from_atlaslabel

scratch_dir = '~/.scratch/drlfom'

def assign_groups(df, groups,
	no='Vehicle',
	yes='Fluoxetine',
	group_subject_key='ETH/AIC',
	):
	subjects = df['subject'].unique()
	df['treatment'] = ''
	for subject in subjects:
		meta = groups.loc[(groups[group_subject_key] == int(subject))]
		if list(meta['Cage_TreatmentProtocol_code'].unique()) == ['cFluDW']:
			df.loc[
				(df['subject']==subject) &
				(df['session'].isin(['ofM','ofMcF1','ofMcF2','ofMpF'])),
				'treatment'] = yes
		elif list(meta['Cage_TreatmentProtocol_code'].unique()) == ['cFluDW_']:
			df.loc[
				(df['subject']==subject) &
				(df['session'].isin(['ofMcF1','ofMcF2','ofMpF'])),
				'treatment'] = no
		if list(meta['TreatmentProtocol_code'].unique()) == ['aFluIV']:
			df.loc[
				(df['subject']==subject) &
				(df['session'].isin(['ofM','ofMaF'])),
				'treatment'] = yes
		if list(meta['TreatmentProtocol_code'].unique()) == ['aFluIV_']:
			df.loc[
				(df['subject']==subject) &
				(df['session'].isin(['ofM','ofMaF'])),
				'treatment'] = no
		if list(meta['TreatmentProtocol_code'].unique()) == ['aFluSC']:
			df.loc[
				(df['subject']==subject) &
				(df['session'].isin(['ofMaF'])),
				'treatment'] = 'Fluoxetine (SC)'
			if list(meta['Cage_TreatmentProtocol_code'].unique()) == ['cFluDW_']:
				df.loc[
					(df['subject']==subject) &
					(df['session'].isin(['ofM'])),
					'treatment'] = no
		if meta['Genotype_code'].unique == ['epwt']:
			df.loc[(df['subject']==subject), 'exclude'] = True
	return df

# Compute irregularities filter
reject_irregularities = [
	'maintained isoflurane at 1.5%% during measurement',
	'incomplete Endorem delivery',
	'slightly incomplete Endorem delivery',
	]
dw_dir = '{}/dw_bids'.format(scratch_dir)
dw_irregularities = session_irregularity_filter(dw_dir, reject_irregularities)
iv_dir = '{}/iv_bids'.format(scratch_dir)
iv_irregularities = session_irregularity_filter(iv_dir, reject_irregularities)
irregularities = pd.concat([iv_irregularities,dw_irregularities])

# Load groups
groups = pd.read_csv('../data/groups.csv')

# Select source data
## DW
t_df = bids_autofind_df('{}/dw_l1_seed/'.format(scratch_dir),
	path_template='sub-{{subject}}/ses-{{session}}/'\
		'sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-{{acquisition}}_run-{{run}}_{{contrast_label}}_tstat.nii.gz',
	match_regex='.+sub-(?P<sub>.+)/ses-(?P<ses>.+)/'\
		'.*?_task-(?P<task>.+)_acq-(?P<acquisition>.+)_run-(?P<run>.+)_(?P<contrast_label>cbv|bold)_tstat\.nii\.gz',
	)
t_df = t_df.rename(
	columns={
		'path':'t-stat path',
		})

p_df = bids_autofind_df('{}/dw_l1_seed/'.format(scratch_dir),
	path_template='sub-{{subject}}/ses-{{session}}/'\
		'sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-{{acquisition}}_run-{{run}}_{{contrast_label}}_pstat.nii.gz',
	match_regex='.+sub-(?P<sub>.+)/ses-(?P<ses>.+)/'\
		'.*?_task-(?P<task>.+)_acq-(?P<acquisition>.+)_run-(?P<run>.+)_(?P<contrast_label>cbv|bold)_pstat\.nii\.gz',
	)
p_df = p_df.rename(
	columns={
		'path':'p-stat path',
		})
dw_source_df = pd.merge(t_df, p_df,)
dw_source_df = assign_groups(dw_source_df,groups)
## IV
t_df = bids_autofind_df('{}/iv_l1_seed/'.format(scratch_dir),
	path_template='sub-{{subject}}/ses-{{session}}/'\
		'sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-{{acquisition}}_run-{{run}}_{{contrast_label}}_tstat.nii.gz',
	match_regex='.+sub-(?P<sub>.+)/ses-(?P<ses>.+)/'\
		'.*?_task-(?P<task>.+)_acq-(?P<acquisition>.+)_run-(?P<run>.+)_(?P<contrast_label>cbv|bold)_tstat\.nii\.gz',
	)
t_df = t_df.rename(
	columns={
		'path':'t-stat path',
		})

p_df = bids_autofind_df('{}/iv_l1_seed/'.format(scratch_dir),
	path_template='sub-{{subject}}/ses-{{session}}/'\
		'sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-{{acquisition}}_run-{{run}}_{{contrast_label}}_pstat.nii.gz',
	match_regex='.+sub-(?P<sub>.+)/ses-(?P<ses>.+)/'\
		'.*?_task-(?P<task>.+)_acq-(?P<acquisition>.+)_run-(?P<run>.+)_(?P<contrast_label>cbv|bold)_pstat\.nii\.gz',
	)
p_df = p_df.rename(
	columns={
		'path':'p-stat path',
		})
iv_source_df = pd.merge(t_df, p_df,)
iv_source_df = assign_groups(iv_source_df,groups)

# Compute seed assigned Cluster activity
assignment = '../data/dw_l2_seed/assignment_4_spherical.nii.gz'
for i in range(4):
	i+=1
	myroi = roi_from_atlaslabel(assignment,[i])
	## DW
	df_dr = deepcopy(dw_source_df)
	df_dr = df_roi_data(df_dr, myroi,
		column_string='Cluster t',
		path_column='t-stat path',
		)

	df = df_significant_signal(df_dr, myroi,
		column_string='Cluster Significance',
		exclude_ones=True,
		path_column='p-stat path',
		)

	df = pd.merge(df, irregularities, how='left')
	df = df.drop_duplicates()
	df.columns = map(str.title, df.columns)
	df = df.rename(
		columns={
			'Modality':'Contrast',
			'Mean Cluster T':'Mean Cluster t',
			'Median Cluster T':'Median Cluster t',
			})
	df.to_csv('../data/dw_seed_{}.csv'.format(i))

	## IV
	df_dr = deepcopy(iv_source_df)
	df_dr = df_roi_data(df_dr, myroi,
		column_string='Cluster t',
		path_column='t-stat path',
		)

	df = df_significant_signal(df_dr, myroi,
		column_string='Cluster Significance',
		exclude_ones=True,
		path_column='p-stat path',
		)

	df = pd.merge(df, irregularities, how='left')
	df = df.drop_duplicates()
	df.columns = map(str.title, df.columns)
	df = df.rename(
		columns={
			'Modality':'Contrast',
			'Mean Cluster T':'Mean Cluster t',
			'Median Cluster T':'Median Cluster t',
			})
	df.to_csv('../data/iv_seed_{}.csv'.format(i))
