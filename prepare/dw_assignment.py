import nibabel as nib
import pandas as pd
from joblib import delayed, Parallel
from samri.analysis.segmentation import assignment_from_paths
from samri.utilities import bids_autofind_df

def assign(repetition,
	format_str='',
	):
	results = []
	for components in [i+1 for i in range(15)]:
		for covariance in ['spherical', 'diag', 'tied', 'full']:
			d = {}
			_, reliability, aic1, aic2, bic1, bic2 = assignment_from_paths(paths,
				components=components,
				covariance=covariance,
				save_as='../data/dw_l2{}/assignment_{}_{}.nii.gz'.format(format_str,components,covariance),
				)
			d['Components'] = components
			d['Covariance'] = covariance
			d['Reliability [%]'] = reliability
			d['Repetition'] = repetition
			d['AIC_1'] = aic1
			d['AIC_2'] = aic2
			d['BIC_1'] = bic1
			d['BIC_2'] = bic2
			results.append(d)
	return results

scratch_dir = '../data'
n_procs = 25
repetitions = 5

df = bids_autofind_df('{}/dw_l2_seed/'.format(scratch_dir),
        path_template='ses-{{session}}/'\
                'acq-{{acquisition}}_run-0_tstat.nii.gz',
        match_regex='.+ses-(?P<ses>.+)/'\
                '.*?acq-(?P<acquisition>.+)_run-0_tstat\.nii\.gz',
        )
paths = df['path'].to_list()

res = Parallel(n_jobs=n_procs, verbose=0, backend="threading")(map(delayed(assign),
	[i for i in range(repetitions)],
	['_seed']*repetitions,
	))
res = [item for sublist in res for item in sublist]
res_df = pd.DataFrame(res)
res_df.to_csv('../data/dw_seed_assignment_reliabilities.csv')

df = bids_autofind_df('{}/dw_l2/'.format(scratch_dir),
        path_template='ses-{{session}}/'\
                'acq-{{acquisition}}_run-0_tstat.nii.gz',
        match_regex='.+ses-(?P<ses>.+)/'\
                '.*?acq-(?P<acquisition>.+)_run-0_tstat\.nii\.gz',
        )
paths = df['path'].to_list()

res = Parallel(n_jobs=n_procs, verbose=0, backend="threading")(map(delayed(assign),
	[i for i in range(repetitions)],
	))
res = [item for sublist in res for item in sublist]
res_df = pd.DataFrame(res)
res_df.to_csv('../data/dw_assignment_reliabilities.csv')
