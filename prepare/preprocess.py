from samri.pipelines.preprocess import generic, legacy
from samri.pipelines import manipulations

scratch_dir = '~/.scratch/drlfom'

# Preprocess drinking water data:
bids_base = '{}/dw_bids'.format(scratch_dir)
generic(bids_base,
	'/usr/share/mouse-brain-templates/dsurqec_200micron.nii',
	registration_mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	sessions=['ofM','ofMaF','ofMcF1','ofMcF2','ofMpF'],
	functional_match={'acquisition':['EPI'],},
	structural_match={'acquisition':['TurboRARE'],},
	out_base=scratch_dir,
	workflow_name='dw_preprocessing',
	)
# Preprocess intravenous data:
bids_base = '{}/iv_bids'.format(scratch_dir)
generic(bids_base,
	'/usr/share/mouse-brain-templates/dsurqec_200micron.nii',
	registration_mask='/usr/share/mouse-brain-templates/dsurqec_200micron_mask.nii',
	sessions=['ofM','ofMaF','ofMcF1','ofMcF2','ofMpF'],
	functional_match={'acquisition':['EPIlowcov'],},
	structural_match={'acquisition':['TurboRARElowcov'],},
	out_base=scratch_dir,
	workflow_name='iv_preprocessing',
	)

