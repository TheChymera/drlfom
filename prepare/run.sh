if [ ! -d ~/.scratch ]; then
	echo "You seem to be lacking a ~/.scratch/ directory."
	echo "We need this directory in order to process the data, and it needs to be on a volume with 200GB+ space."
	echo "You can simply symlink to a location you would like this to happen (and then re-run this script):
		ln -s /where/you/want/it ~/.scratch"
	exit 1
fi

if [ ! -d ~/.scratch/drlfom/dw_bids ]; then
	if [ -d "/usr/share/drlfom_bidsdata" ]; then
		[ -d ~/.scratch/drlfom ] || mkdir ~/.scratch/drlfom
		ln -s /usr/share/drlfom_bidsdata ~/.scratch/drlfom/dw_bids
	else
		echo "No DRLFOM drinking water BIDS data distribution found, processing from scanner DRLFOM data:"
		python make_dw_bids.py
	fi
fi
if [ ! -d ~/.scratch/drlfom/iv_bids ]; then
	if [ -d "/usr/share/irsabi_bidsdata" ]; then
		ln -s /usr/share/irsabi_bidsdata ~/.scratch/drlfom/iv_bids
	else
		echo "No DRLFOM intravenous (i.e. IRSABI) BIDS data distribution found, processing from scanner DRLFOM data:"
		python make_iv_bids.py
	fi
fi

if [[ ! -f '../data/groups.csv' && -f '../data/timetable_dw_pre.csv' && -f '../data/timetable_dw.csv' && -f '../data/irregularities.csv' ]]; then
	python metadata.py || exit 1
fi

python preprocess.py || exit 1
python l1.py || exit 1
python activity.py || exit 1
python seed.py || exit 1
python l2.py || exit 1
python dw_assignment.py || exit 1
python assignment_seed.py || exit 1
python assignment_activity.py || exit 1
rsync -avP --exclude='*_cope.nii*' --exclude='*_zstat.nii*' ~/.scratch/drlfom/*l2* ../data/
