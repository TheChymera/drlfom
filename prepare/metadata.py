from labbookdb.report.tracking import append_external_identifiers, overview, treatment_group
from labbookdb.report.selection import animal_id, parameterized
from labbookdb.report.utilities import relativize_dates
from labbookdb.report.development import animal_multiselect

from os import path
import pandas as pd
import numpy as np

db_path='~/syncdata/meta.db'

# Animal treatment metadata
df = treatment_group(db_path, ['aFluIV','aFluIV_','aFluSC','aFluSC_','cFluIP'], level='animal')
df = append_external_identifiers(db_path, df, ['Genotype_code'])
df = df.drop('Animal_death_date', axis=1)
df = df.groupby('Animal_id').transform(lambda x: x.bfill().ffill())
df.to_csv('../data/groups.csv')

# Timetable metadata
subjects = animal_multiselect(db_path,
	cage_treatments=['cFluDW','cFluDW_'],
	implant_targets=['dr_impl'],
	virus_targets=['dr_skull','dr_dura','dr_dura_shallow','dr_skull_perpendicular'],
	genotypes=['eptg'],
	)
myfilter = ["Animal","id",]
myfilter.extend(subjects)
filters = [myfilter]

timetable_df = overview(db_path,
	filters=filters,
	default_join="outer",
	join_types=["outer","outer","outer","outer","outer","outer","outer","outer","outer","outer"],
	relative_dates=False,
	)
timetable_df = timetable_df.rename(columns={'Animal_id':'subject'})
timetable_df['subject'] = timetable_df['subject'].apply(lambda x: animal_id('~/syncdata/meta.db','ETH/AIC',str(x),reverse=True))
timetable_df.to_csv('../data/timetable_dw_pre.csv')
timetable_df = relativize_dates(timetable_df,
	rounding='D',
	rounding_type='round',
	)
timetable_df.to_csv('../data/timetable_dw.csv')

# Scan irregularity metadata
subjects = animal_multiselect(db_path,
	implant_targets=['dr_impl'],
	virus_targets=['dr_skull','dr_dura','dr_dura_shallow','dr_skull_perpendicular'],
	genotypes=['eptg'],
	)
irregularities_df = parameterized(db_path,'animals measurements irregularities',animal_filter=subjects)
irregularities_df.to_csv('../data/irregularities.csv')
