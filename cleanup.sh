#!/usr/bin/env bash

rm -rf \
	auto_fig_py*.png \
	auto_fig_py*.pdf \
	*.aux \
	*.bbl \
	*.bcf \
	*.blg \
	*.depytx \
	*.log _minted-slides \
	*.nav \
	*.out \
	*.pgf \
	pythontex-files-* \
	*.pkl \
	*.pytxcode \
	*.run.xml \
	*.snm \
	*.toc \
	*.vrb 
