\section{Discussion}

%In this article we reproduce previous baseline serotonergic opto-fMRI results from literature (\cref{fig:ofmm,fig:ofmn,fig:ofmp}), and iteratively interrogate the system over a longitudinal course of fluoxetine treatment.
In this article we interrogate the serotonergic system in the mouse over a longitudinal course of fluoxetine treatment, by using optogenetic stimulation of serotonergic DR neurons in conjunction with fMRI.
Activation patterns observed at baseline (\cref{fig:ofmm,fig:ofmn,fig:ofmp}) were analogous to those reported in a previous study \cite{Grandjean2019}.
We observe that the system shows no lateral preference in its functional effects, neither at baseline, nor during fluoxetine treatment or after treatment cessation.

Our results suggest that fluoxetine treatment induces functional serotonergic effects corroborating the autoinhibition down-regulation theory.
This is best represented by the differential temporal trajectories of the DR (\cref{fig:dd}) and the cortical projection areas (\cref{fig:d3}).
In this comparison, the DR becomes significantly more sensitive to stimulation in the chronic, but not the acute administration sessions.
In contrast, the cortical projection areas show significantly larger negative signal amplitudes for the acute and 2-week chronic, but not the 4-week chronic sessions.

In order to identify coherent trajectories at the whole brain level we use unsupervised machine learning to determine an appropriate segmentation of voxels into clusters, based on their temporal profile.
Acknowledging the limitations inherent in unsupervised model selection, we inspect classification reliability and further provide the cluster assignment maps for all tested models in volumetric form for download \cite{me}.
The clusters identified using the most reliable classification parameters show strong spatial coherence, and roughly follow anatomical features (\cref{fig:gmm4}).
We name these trajectory-based clusters “brainstem”, “subcortical”, and “cortical”.

The cortical trajectory cluster encompasses primarily cortical regions (\cref{fig:3m,fig:3dist}), with its temporal profile showing a significant fluoxetine effect upon acute fluoxetine administration, and again in the 2-week chronic administration session.
All effects manifest themselves as enhanced inhibition, with the acute effect being notably the highest in amplitude.
This is consistent with homeostatic adaptation of the serotonergic system, in the context of which acute exposure to the drug leads to accumulation of neurotransmitter at the synapse and strengthens the inhibitory effect --- whereas during chronic administration the synapse adjusts to elicit a postsynaptic effect more consistent with the baseline.
Whether this homeostatic effect converges on the baseline or an intermediary stable state (as suggested by the intraperitoneal administration dataset, \cref{fig:d3}) is unclear given the duration of chronic administration.

The brainstem trajectory cluster encompasses both the DR and the majority of the rest of the brainstem, the part of the cerebellum captured during data acquisition, some of the ventralmost regions of the forebrain along the midline (\cref{fig:2m}), and notably, the fimbria (\cref{fig:gmm4}).
The trajectory of this cluster strongly resembles that of the DR, though at a roughly \SI{50}{\percent} reduced amplitude (\cref{fig:i2}).
Interestingly, in this case, significant fluoxetine effects are also found for the acute session.
These results are not strictly consistent with the autoinhibition down-regulation theory, but also not necessarily at odds with it.
The existence of such a trajectory cluster (including but also extending far beyond the DR) indicates that there are numerous brain areas other than the DR, in which the same sensitization effect can be observed.
One possible explanation is that these areas do not experience homeostatic adaptation to increased serotonergic signalling at the synapse, and consequently mirror the same signal behaviour as their afferents.
Interestingly, this is the only cluster showing positive signal transmission, and does so only during the treatment window.
It can thus be speculated that excitatory serotonergic synapses experience a distinctly evolution during fluoxetine treatment.

The subcortical trajectory cluster is the least spatially coherent, but shows some anatomical alignment with striatal and limbic regions (\cref{fig:1m,fig:1dist}).
Though it is the only cluster to extend into the hippocampus proper (the brainstem cluster distinctly covers the fimbria), coverage is sparse, predominantly restricted to the granular layer (\cref{fig:gmm4}).
Overall, the temporal trajectory is reminiscent of that shown by the cortical cluster, albeit with lower amplitudes, and only showing a significant effect in the acute administration session.
The same homeostatic mechanism as for the cortical cluster can be proposed here.
Considering its looser spatial arrangement, however, an alternative explanation can be put forward.
Given the strong spatial autocorrelation of fMRI signals, it is possible that this cluster simply captures the intermediary effect between the cortical cluster and the background.

As expected, neither the DR region of interest, nor the trajectory clusters show a significant treatment group contrast for the naïve session.
This and the stability of activity across regions of interest in the control condition strongly support the robustness of the assay and reliability of the effects observed.
While the assay is able to discern fluoxetine treatment effects with region-specific temporal trajectories, neither the DR nor the trajectory clusters show a significant treatment contrast two weeks after fluoxetine withdrawal \cref{fig:post}.
We thus conclude that in healthy mice there is no post-treatment fluoxetine effect of similar nature or amplitude to either the acute or chronic treatment effects.

In comparing the drinking water and intraperitoneal chronic administration experiments, we note that only the trajectories of the cortical and subcortical clusters are reproducible.
Noting the more limited resolution, more sparse slice positioning, and reduced rostrocaudal coverage, we attribute this in part to the incomplete capture of the DR and brainstem activity profiles.
Consequently, we recommend high rostrocaudal coverage with dense slice positioning for robust longitudinal opto-fMRI.
Such acquisition improves statistic reliability both by eliminating slice gaps, and by improving co-registration stability.
A decreased stability of fluoxetine treatment effects under intraperitoneal chronic administration cannot be ruled out, though it seems less plausible that drug administration procedure effects (e.g. animal stress) would specifically degrade the statistical estimates of the brainstem trajectory.

In this study, seed-based analysis is explored as a modelling alternative for improved differentiation of serotonergic cell excitability and serotonergic synapse transmission efficiency.
Given variable levels of activation at the primary stimulation site, increased signal in projection areas cannot unambiguously be attributed to more efficient transduction at the synapse.
The theoretical expectation is that projection area signal in a seed-based evaluation would be blunted for sessions in which the stimulation site activation is strongest (i.e. the 2-week and 4-week treatment sessions, \cref{fig:dd}).
However, seed-based analysis of the data at hand shows an opposite effect, whereby it is primarily the acute treatment session for which the projection area response is blunted (comparing \cref{fig:sd3} and \cref{fig:d3}).
Overall, seed based connectivity shows reduced statistical estimates \cref{fig:ofms} and less distinct longitudinal trajectories (\cref{fig:s}).
This may indicate that the inability of seed-based connectivity to deliver meaningful disambiguation of excitability and transmission in this study is simply an issue of susceptibility to noise in the stimulation area regressor.
We conclude that resolving network dynamics is an important endeavour for stimulus-evoked fMRI ---
yet may require additional methodological improvements in order to afford insight in excess of stimulus-evoked response modelling.


\subsection{Summary}

We have applied an emerging opto-fMRI assay to inspect longitudinal changes in serotonergic function elicited by acute and chronic treatment with the SSRI fluoxetine.
The analysis revealed three trajectory clusters that follow two different patterns consistent with the autoinhibition down-regulation theory, and further clarifies the spatial distribution of fluoxetine treatment effects.
We have shown that the most salient of these trajectories can be observed in two distinct datasets with numerous parameter variations (including drug delivery and MRI acquisition).
In particular, we have demonstrated that while treatment effects are highly significant, no persistent changes are seen after treatment cessation in healthy mice.
This provides evidence against the proposition that fluoxetine induces stable homeostatic shifts persisting beyond the treatment period.
Such a statement can speak both for and against fluoxetine administration (and perhaps SSRI administration in general), depending on whether an intervention time window or a persistent shift in function is the desired result.

We suggest that opto-fMRI based analysis of serotonergic signaling constitutes an attractive assay for profiling serotonergic drugs based on spatio-temporally resolved, whole-brain neurotropic effects.
The high level of differentiation between effects induced by fluoxetine, a representative of the SSRI drug class, as compared to the vehicle illustrates the potential of the approach.
However, it remains to be determined whether the method provides the sensitivity required to robustly distinguish the responses elicited by different serotonergic drugs recommended for chronic administration (such as different SSRIs).

In the interest of transparency and technology access we provide both the code required to perform all the analyses in this article --- as well as the resulting summaries and cluster classifications --- for public access \cite{me}.
