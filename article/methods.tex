\section{Materials and Methods}

\subsection{Animal Preparation}

DR serotonergic neurons were specifically targeted via optogenetic stimulation.
As shown in \cref{fig:og}, this process entails a three-stage selection process: the cell-type selection based on the transgenic mouse strain, the location selection based on the injection site, and the activation based on the overlap of the aforementioned selection steps with the optic fiber tip.
The study was performed in mice expresses Cre recombinase under a Pet-1 transcription factor enhancer (ePet), which is uniquely expressed in serotonergic neurons \cite{Scott2005}.
Construct presence was assessed via polymerase chain reaction (PCR) of a Cre gene segment, using the forward primer ACCAGCCAGCTATCAACTCG and the reverse primer TTGCCCCTGTTTCACTATCC.

The DR of the animals was injected with a solution containing recombinant Adeno-Associated Viruses (rAAVs).
The injection entry point was \SI{0.6}{\milli\meter} caudal and \SI{1}{\milli\meter} right of lambda, with the cannula being inserted to a depth of \SI{3.6}{\milli\meter} from the skull at a yaw of \SI{20}{\degree} towards the left of the dorsoventral axis.
The vector delivered a plasmid containing a floxed channelrhodopsin and YFP construct:
pAAV-EF1a-double floxed-hChR2(H134R)-EYFP-WPRE-HGHpA, gifted to a public repository by Karl Deisseroth (\href{https://www.addgene.org/20298/}{Addgene plasmid \#20298}).
Viral vectors and plasmids were produced by the Viral Vector Facility (VVF) of the Neuroscience Center Zurich (Zentrum für Neurowissenschaften Zürich, ZNZ).
The solution was prepared at a titer of \SI{5.7e12}{\vg\per\milli\litre} and the injection volume was \SI{1.3}{\micro\litre}.
Construct expression was ascertained post mortem via fluorescence microscopy of formaldehyde fixated \SI{200}{\micro\metre} brain slices.

Animals were fitted with an optic fiber implant ($\mathrm{l=\SI{3.3}{\milli\meter}, \ d=\SI{400}{\micro\meter}, \ NA=0.22}$) inserted perpendicularly, to its full length, at \SI{0.6}{\milli\meter} caudal of lambda and on the midline.

All animal work was performed in accordance with relevant animal welfare regulations as implemented by the Cantonal Veterinary Office of Zurich.

\subsection{Drug Treatments}
The effects of acute and chronic fluoxetine treatment were evaluated based both on novel drinking water administration data published herein, and on previously published (but, as of yet, unevaluated) intraperitoneal administration data \cite{irsabi}.
A graphical overview for the time courses of both these treatments can be found in \cref{fig:t}.
For both chronic treatment experiments, fluoxetine hydrochloride (\SI{\geq99}{\percent} HPLC, Bio-Techne AG, Switzerland) was used to prepare an injection solution (\SI{2.25}{\milli\gram\per\milli\litre} in saline).
For the acute fluoxetine administration session of both treatment experiments, a volume of the solution adjusted to deliver fluoxetine at \SI{10}{\mg\per\kg\per\BW} was injected intravenously \SI{10}{\minute} prior to functional scan acquisition.

%\begin{sansmath}
%\begin{figure*}[h!]
%	\centering
%	\hspace*{\fill}
%	\begin{subfigure}{.98\textwidth}
%		\centering
%		\includedot[width=1.015\textwidth]{data/dw_timetable}
%		\vspace{-0.9em}
%		\caption{
%			Drinking water delivery experiment timecourse, with days where animals had access to unadulterated drinking water marked in blue, and days where animals drank fluoxetine solution marked in orange.
%			}
%		\label{fig:dw_t}
%	\end{subfigure}
%	\begin{subfigure}{.98\textwidth}
%		\centering
%		\includedot[width=1.015\textwidth]{data/iv_timetable}
%		\vspace{-0.9em}
%		\caption{
%			Intraperitoneal delivery experiment timecourse, with days where animals received no injections marked grey, and days where animals received fluoxetine injections marked orange.
%			}
%		\label{fig:iv_t}
%	\end{subfigure}
%	\caption{
%		Schematic of the longitudinal experiment timetable design.
%		Opto-fMRI measurement days are denoted via box nodes, and acute drug delivery for the experiment is noted inside the boxes.
%		Grey outer cassettes denote the session identifier names for the study.
%		}
%	\label{fig:t}
%\end{figure*}
%\end{sansmath}

	\subsubsection{Chronic Drinking Water Administration Experiment}
	Fluoxetine hydrochloride (\SI{\geq99}{\percent} HPLC, Bio-Techne AG, Switzerland) was used to prepare a drinking water (\SI{124}{\milli\gram\per\litre} in tap water) solution.
	Drinking behaviour was monitored to ascertain daily fluoxetine consumption of not fewer than \SI{15}{\milli\gram\per\kilo\gram\per\BW} and the intravenous solution volume was adjusted to deliver fluoxetine at \SI{10}{\mg\per\kg\per\BW}.
	Drinking water during and prior to the experiment was delivered from bottles wrapped in aluminium foil.
	The drinking solution as well as the fodder, cage, and bedding were replaced weekly --- in synchrony with the measurements (after each measurement, and at corresponding intervals between measurements).
	This dataset includes fMRI recordings from
	\sessionpy{boilerplate.subject_counter(criteria={'Treatment':['Fluoxetine'],'Exclude':[False]})} animals subjected to fluoxetine treatment
	and \sessionpy{boilerplate.subject_counter(criteria={'Treatment':['Vehicle'],'Exclude':[False]})} animals serving as vehicle administration controls.

	\subsubsection{Chronic Intraperitoneal Administration Experiment}
	Over the course of the chronic treatment period (\cref{fig:iv_t}), the fluoxetine injection solution was delivered intraperitoneally once a day, every day.
	The volume for chronic (intraperitoneal) injections was adjusted to deliver fluoxetine at \SI{10}{\mg\per\kg\per\BW}.
	This dataset includes fMRI recordings from \sessionpy{boilerplate.subject_counter(data_path='data/iv_activity_dr.csv',criteria={'Exclude':[False]})} animals, all subjected to fluoxetine treatment.

\subsection{MR Acquisition}

Over the course of preparation and measurement, animals were provided a constant flow of air with an additional \SI{20}{\percent} $\mathrm{O_2}$ gas (yielding a total $\mathrm{O_2}$ concentration of \SI{\approx 36}{\percent}).
For animal preparation, anesthesia was induced with \SI{3}{\percent} isoflurane, and maintained at \SIrange{2}{3}{\percent} during preparation --- contingent on animal reflexes.
Animals were fixed to a heated MRI-compatible cradle via ear bars and a face mask equipped with a bite hook.
A subcutaneous (s.c.; right dorsal) and intravenous (i.v.; tail vein) infusion line were applied.
After animal fixation, a bolus of medetomidine hydrochloride (Domitor, Pfizer Pharmaceuticals, UK) was delivered s.c. to a total dose of \SI{100}{\nano\gram\per\gram\per\BW} and the inhalation anesthetic was reduced to \SI{1.5}{\percent} isoflurane.
After a \SI{5}{\minute} interval, the inhalation anesthetic was set to \SI{0.5}{\percent} and medetomidine was continuously delivered at \SI{200}{\nano\gram\per\gram\per\BW\per\hour} for the duration of the experiment.
This anesthetic protocol is closely based on extensive research into animal preparation for fMRI \cite{Grandjean2014}.

All data were acquired with a Bruker Biospec system (\SI{7}{\tesla}, \SI{16}{\centi\meter} bore), and an in-house built transmit/receive surface coil, engineered to permit optic fiber implant protrusion.


	\subsubsection{Chronic Drinking Water Administration Experiment}
	Anatomical scans were acquired via a TurboRARE sequence, with a RARE factor of 8, an echo time (TE) of \SI{30}{\milli\second}, an inter-echo spacing of \SI{10}{\milli\second}, and a repetition time (TR) of \SI{2.95}{\second}.
	Thirty adjacent (no slice gap) coronal slices were recorded with a nominal in-plane resolution of $\mathrm{\Delta x(\nu)=\Delta y(\phi)=\SI{75}{\micro\meter}}$, and a slice thickness of $\mathrm{\Delta z(t)=\SI{450}{\micro\meter}}$.

	Functional scans were acquired with a gradient-echo EPI sequence, a flip angle of \SI{60}{\degree}, and $\mathrm{TR/TE = \SI{1000}{\milli\second}/\SI{5.9}{\milli\second}}$.
	Thirty adjacent (no slice gap) coronal slices were recorded with a nominal in-plane resolution of $\mathrm{\Delta x(\nu)=\Delta y(\phi)=\SI{225}{\micro\meter}}$, and a slice thickness of $\mathrm{\Delta z(t)=\SI{450}{\micro\meter}}$.
	Changes in cerebral blood volume (CBV) are measured as a proxy of neuronal activity following the administration of an intravascular iron oxide nanoparticle based contrast agent (Endorem, Laboratoire Guebet SA, France).
	The contrast agent ($\SI{30.24}{\micro\gram\per\gram\per\BW}$) was delivered as an i.v. bolus \SI{10}{\minute} prior to the fMRI data acquisition, to achieve a pseudo steady-state blood concentration.
	This contrast is chosen to enable short echo-time imaging thereby minimizing artefacts caused by gradients in magnetic susceptibility \cite{Mandeville2004}.

	The DR was stimulated via an Omicron LuxX 488-60 laser (\SI{488}{\nano\meter}) tuned to a power of \SI{30}{\milli\watt} at contact with the fiber implant, according to the protocol listed in \cref{tab:CogB}.

	\subsubsection{Chronic Intraperitoneal Administration Experiment}
	The acquisition parameters of this dataset are detailed in the original publication \cite{irsabi}.
	The DR was stimulated via an Omicron LuxX 488-60 laser (\SI{488}{\nano\meter}) tuned to a power of \SI{30}{\milli\watt} at contact with the fiber implant, according to the protocol listed in \cref{tab:JogB}.

\subsection{Data Processing}
Stimulation protocols were delivered to the laser and recorded to disk via the COSplayer device \cite{cosplay}.
Animal physiology, preparation, and measurement metadata were tracked with the LabbookDB database framework \cite{ldb}.

Data conversion from the proprietary ParaVision format was performed via the Bruker-to-BIDS repositing pipeline \cite{aowsis} of the SAMRI package (version \textcolor{mg}{\texttt{0.4}} \cite{samri}).
Following conversion, data was dummy-scan corrected, registered, and subject to controlled smoothing via the SAMRI Generic registration workflow \cite{irsabi}.
As part of this processing, the first 10 volumes were discarded (automatically accounting for volumes excluded by the scanner software).
Registration was performed using the standard SAMRI mouse-brain-optimized parameter set for ANTs \cite{ants} (version \textcolor{mg}{\texttt{2.3.1}}).
Data was transformed to a stereotactically oriented standard space (\textcolor{mg}{\texttt{dsurquec}}, as distributed in the Mouse Brain Atlases Package \cite{atlases_generator}, version \textcolor{mg}{\texttt{0.5.3}}), which is based on a high-resolution $\mathrm{T_2}$-weighted atlas \cite{dsu1}.
Controlled spatial smoothing was applied in the coronal plane up to \SI{250}{\micro\meter} via the AFNI package \cite{afni} (version \textcolor{mg}{\texttt{19.3.12}}).

The registered time course data was frequency filtered depending on the analysis workflow.
For stimulus-evoked activity, the data was low-pass filtered at a period threshold of \SI{225}{\second}, and for seed-based functional connectivity, the data was band-pass filtered within a period range of \SIrange{2}{225}{\second}.

\subsection{Statistics}
Volumetric data was modelled using functions from the FSL software package \cite{fsl} (version \textcolor{mg}{\texttt{5.0.11}}).
First-level regression was applied to the temporally resolved volumetric data via FSL's \textcolor{mg}{\texttt{glm}} function, whereas the second-level analysis was applied to the first-level contrast and variance estimates via FSL's \textcolor{mg}{\texttt{flameo}}.

Stimulus-evoked first-level regression was performed using a convolution of the stimulus sequence with an opto-fMRI impulse response function, estimated by a beta fit of previously reported mouse opto-fMRI responses \cite{Grandjean2019}.
Seed-based functional connectivity analysis was performed by regressing the time course of the DR voxel most sensitive to stimulus-evoked activity (per scan).

Brain parcellation for region-based evaluation was performed using a non-overlapping multi-center labelling \cite{dsu1,dsu2,dsu3,dsu4}, as distributed in version \textcolor{mg}{\texttt{0.5.3}} of the Mouse Brain Atlases data package \cite{atlases_generator}.
The mapping operations were performed by a SAMRI function, using the nibabel \cite{nibabel} and nilearn \cite{nilearn} libraries (versions \textcolor{mg}{\texttt{2.3.1}} and \textcolor{mg}{\texttt{0.5.0}}, respectively).
Gaussian mixture modelling for voxel classification was performed via the \textcolor{mg}{\texttt{GaussianMixture}} class from the scikit-learn library \cite{scikit-learn} (version \textcolor{mg}{\texttt{0.20.3}}).
Distribution density visualizations were created using the Scott bandwidth density estimator \cite{Scott1979}.
For the visualization of parcellation-based statistic score density distributions, regions with a volume lower than \SI{0.06}{\cubic\milli\meter} or with more than \SI{34}{\percent} exact zero values are excluded from analysis.

Higher-level statistical modelling was performed with the Statsmodels software package \cite{statsmodels} (version \textcolor{mg}{\texttt{0.9.9}}), and the SciPy software package \cite{scipy} (version \textcolor{mg}{\texttt{1.1.0}}).
Model parameters were estimated using the ordinary least squares method, and a type 3 analysis of variance (ANOVA) was employed to control estimate variability for unbalanced categories.
All t-tests producing explicitly noted p-values are two-tailed, and all post-hoc t-tests use the Benjamini-Hochberg procedure, controlling the false discovery rate (FDR) at $\mathrm{\alpha\!=\!0.05}$.

Software management relevant for the exact reproduction of the aforementioned environment was performed via neuroscience package install instructions for the Gentoo Linux distribution \cite{ng}.

\subsection{Reproducibility and Open Data}

The resulting t-statistic maps, as well as the GMM cluster assignment maps (both selected and rejected) are distributed along the source-code of all analyses \cite{me}.
The BIDS \cite{bids} data archives which serve as raw data recourse for this document are openly distributed \cite{drlfom_bidsdata,irsabi_bidsdata}, as is the full instruction set for recreating this document \cite{me}.
The source code for this document and all data analysis shown herein is published according to the RepSeP specifications \cite{repsep}.
The data analysis execution and document compilation has been tested repeatedly on numerous machines, and as such we attest that the figures and statistics presented can be reproduced based solely on the raw data, dependency list, and analysis scripts which we distribute.
