\section{Introduction}

%\begin{sansmath}
%\begin{figure*}[h!]
%	\vspace{-1.5em}
%	\centering
%	\hspace*{\fill}
%	\begin{subfigure}{.527\textwidth}
%		\centering
%		\includegraphics[width=\textwidth]{img/model_literature}
%		\caption{
%			Schematic map of DR serotonergic projections (overview based on rat data \cite{Steinbusch1981,Lesch2012,PollakDorocic2014}).
%			Projection arrows do not accurately track fiber bundle paths.
%			Abbreviations:
%				Ctx (cortex),
%				Hipp (hippocampus),
%				HT (hypothalamus),
%				OB (olfactory bulb),
%				St (striatum),
%				Th (thalamus).
%			}
%		\label{fig:ma}
%	\end{subfigure}\hfill
%	\begin{subfigure}{.44\textwidth}
%		\centering
%		\vspace{-1em}
%		\includedot[width=1.1\textwidth]{data/network_model}
%		\vspace{-1.48em}
%		\caption{
%			Simplified network model of 1-step signal transduction during DR optogenetic stimulation.
%			The $\mathrm{u_1}$ weighting corresponds to DR somatic excitability and $\mathrm{u_{2a},u_{2b},u_{2c}}$ and $\mathrm{u_{2d}}$ to transmission at the serotonergic synapses in projection areas.
%			}
%		\label{fig:mn}
%	\end{subfigure}
%	\hspace*{\fill}
%	\begin{subfigure}{.985\textwidth}
%		\vspace{.5em}
%		\centering
%		\includegraphics[width=\textwidth]{img/5ht}
%		\vspace{-1.5em}
%		\caption{
%			Schematic cellular and molecular elements of serotonergic neurotransmission.
%			Due to the distance of serotonergic efferent terminals, each neuron's soma is located in the DR, while synapses are located in projection area voxels.
%			Abbreviations:
%				5-HT (serotonin),
%				5-HT$_{1-7}$ (serotonin receptor classes),
%				5-HTP (5-hydroxytryptophan),
%				AC (adenylyl cyclase),
%				IP$_3$ (inositol trisphosphate),
%				MAO (monoamine oxydase),
%				SERT (serotonin transporter),
%				Trp (tryptophan),
%				VMAT (vesicular monoamine transporter)
%				\cite{Torres2003}.
%			}
%		\label{fig:mc}
%	\end{subfigure}
%	\begin{subfigure}{.985\textwidth}
%		\vspace{.5em}
%		\centering
%		\includegraphics[width=0.95\textwidth]{img/optogenetics}
%		\vspace{-.25em}
%		\caption{
%			Schematic of optogenetic cell selection and activation.
%			Green denotes serotonergic cells, gray enlarged elements on the cell periphery indicate channelrhodopsin expression, and cyan segments on the cell periphery denote depolarization events.
%			}
%		\label{fig:og}
%	\end{subfigure}
%	\vspace{-.6em}
%	\caption{
%		\textbf{The cellular compartmentalization of serotonergic neurotransmission and potential pharmacological targets can partly be mapped onto neuroanatomical features by a simple network model, using optogenetics.}
%		Depicted are schematic overviews of the DR ascending serotonergic system at various spatial resolutions and abstraction levels.
%		\vspace{-1em}
%		}
%	\label{fig:m}
%\end{figure*}
%\end{sansmath}
%% Draw voxels as faint squares

The serotonergic system comprises neurons defined by the production and secretion of the neurotransmitter serotonin (5-HT).
This evolutionary highly conserved system, with its hub in the nonlateralized brainstem dorsal raphe nucleus (DR), encompasses around 9000 neurons in the mouse \cite{Ishimura1988} and 11500 in the rat \cite{Descarries1982}.
Though the small number of neurons obscures the system in tractography analysis, it projects to a large subset of brain regions (\cref{fig:ma}), exerting significant control over them \cite{Grandjean2019}, and thus constitutes an important node in the graph representation of the brain.

Consistent with wide-ranging projections, the serotonergic system is implicated in numerous cognitive and behavioural phenomena --- including impulse control, affect, social behaviour, and in particular social dominance \cite{Krakowski2003,Raleigh1991}.
Since most of these phenomena are not under direct cognitive control, exogenous control of the neuronal systems that underpin them is highly sought after.

Perhaps the most prominent phenomenon in which the serotonergic system is implicated is affect, of which the most common dysfunction is depression.
The implication of the serotonergic system in depression is highlighted by serotonin transporter promoter polymorphisms correlating with depression incidence \cite{Pezawas2005}.
This is corroborated by the observation that selective serotonin reuptake is implicated in the etiology of depression \cite{Willeit2007}, and that the inhibition thereof is a viable treatment option for depression \cite{Fava2000}.

In fact, the drugs most commonly used for the treatment of depression are serotonin reuptake inhibitors, such as the selective serotonin reuptake inhibitor (SSRI), fluoxetine \cite{Dulawa2004}.
SSRI treatment owes its success to the targeted and homeostasis-modulating manner in which reuptake inhibition influences serotonergic function.
This is in opposition to direct targeting via receptor agonists or releasing agents --- which can cause extensive nonphysiologic side effects \cite{Ray2010} on account of eliciting activity at a rate only limited by receptor density and distribution, or cause serotonergic depletion \cite{kish2000striatal}, respectively.
In line with the homeostatis-modulating interpretation of SSRIs effects, drugs of this class are documented to only produce therapeutic effects after a 1-2 week period \cite{Lam2012}.

The most prominent theory for serotonergic homeostasis modulation is based on down-regulation of autoinhibition via 5-HT$_1$ class receptors \cite{Moret1990,blier1994}.
This model states that, during SSRI treatment, autoinhibitors chronically receive atypically high feedback signal upon serotonin release.
In response to this signal, autoinhibitors become down-regulated and/or desensitized in their function, thereby increasing the excitability of serotonergic neurons.
This theory is supported by functional as well as protein-expression data \cite{Popa2010,Hensler2002}, but there remains a significant gap in understanding how such a mechanism impacts serotonergic signalling at the whole-brain level.

Assessing whole-brain function in vivo is a challenging task, as most measurement techniques suited to interrogate neurotransmitter or cell-type-specific signalling (microdialysis and electrochemistry, or electrophysiology and optical imaging, respectively) either lack spatial resolution entirely, or are significantly limited in depth penetration.
Functional magnetic resonance imaging (fMRI), while comparatively unspecific in terms of the cellular processes being imaged, provides both sub-millimeter spatial resolution, as well as full brain coverage without the need for invasive intervention, and is thus well-suited for longitudinal application.
While hemodynamic contrast constitutes only an indirect measure of neuronal function, additional specificity can be obtained in animal models by targeting specific neurotransmitter systems via optogenetics.
Further, model animal research (as well as translational integration of evidence across model organisms) is particularly suitable for the serotonergic system, owing to its high evolutionary conservation \cite{Welsh1968}.

%The main aggregation of ascending serotonergic projection cell bodies is the dorsal raphe (DR), a nonlateralized nucleus located ventrally of the sylvian aqueduct along the brain midline.
%This nucleus can be targeted for optogenetic stimulation, which has been shown to be compatible with concurrent fMRI \cite{Grandjean2019}.
%The small number of neurons in the DR (only 9000 in the mouse \cite{Ishimura1988} and 11500 in the rat \cite{Descarries1982}), which in addition do not form large axonal bundles, obscure the structure's salience in tractography analysis.
%Opto-fMRI results, however, reveal that its central role in controlling neuronal function throughout large swathes of the brain \cite{Grandjean2019}.

Serotonergic cell-type specific stimulation in the DR, concurrent with opto-fMRI, can thus be used to highlight neurotransmitter system function at the whole brain level.
This assay can be iterated over a longitudinal drug treatment period, and thus verify whether an SSRI (e.g. fluoxetine) induces meaningful changes on the highlighted system.
Given such time-dependent effects, it can further be ascertained whether they are representative of the therapeutic profile of SSRIs, and compatible with the autoinhibition down-regulation theory.
Given the significant extent to which SSRIs are prescribed, the question of whether and how SSRI treatment affects the function of the healthy brain can also be addressed, by using a healthy model animal population.
Of particular relevance in the context of SSRI effects in healthy subjects, is the question of whether SSRI effects persist post treatment in any of the brain regions.

The interpretation of results with regard to molecular mechanisms is rendered particularly convenient by the notable length of the projections (\cref{fig:ma}).
Given such a spatial distribution, distinct voxels can easily be mapped onto cellular components (\cref{fig:mc}), with DR voxels corresponding to neuronal somata, and projection area voxels corresponding to synaptic compartments.
Based on such a mapping, a simple network model ({\cref{fig:mn}}) can be used to interpret the results of signal propagation from the initial excitatory stimulation delivered to the DR.
In a simple stimulus-evoked analysis, voxel behaviour in the DR thus represents serotonergic cell excitability.
By contrast, voxel behaviour in projection areas represents the sum of serotonergic cell excitability and transmission strength at the serotonergic synapse.
More unambiguous estimates of transmission can be obtained via seed-based functional connectivity, yet the analysis method may be significantly more susceptible to noise than stimulus-evoked analysis \cite{opfvta}.
