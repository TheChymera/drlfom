\documentclass{beamer}
\usepackage[orientation=portrait,size=a0]{beamerposter}
\mode<presentation>{\usetheme{ZH_AIC}}
% \usepackage{chemformula}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
%\usepackage[ngerman]{babel} % required for rendering German special characters
\usepackage{siunitx} %pretty measurement unit rendering
\usepackage[autoprint=false, gobble=auto, pyfuture=all]{pythontex} % create figures on-line directly from python!
\usepackage{hyperref} %enable hyperlink for urls
\usepackage{ragged2e}
\usepackage[font=scriptsize,justification=justified]{caption}
\usepackage{subfig}
\usepackage{booktabs}
\usepackage[noabbrev]{cleveref}
\usepackage{graphviz}

% \sisetup{per=frac,fraction=sfrac}
\sisetup{per-mode=symbol}

\definecolor{elg}{gray}{0.95}
\definecolor{vlg}{gray}{0.90}

\input{/usr/share/repsep/functions.py}
\begin{pythontexcustomcode}[begin]{py}
	DOC_STYLE = 'poster/main.conf'
	pytex.add_dependencies(
		DOC_STYLE,
		'poster/wide.conf',
		'poster/swarmplots.conf',
		'poster/matrices.conf',
		)
\end{pythontexcustomcode}

\usepackage{array,booktabs,tabularx}
\newcolumntype{Z}{>{\centering\arraybackslash}X} % centered tabularx columns

\title{Multimodal opto-fMRI Analysis of Longitudinal SSRI Treatment in Mice}
\author{Horea-Ioan Ioanas$^{1}$, Bechara John Saab$^{2}$, Markus Rudin$^{1}$}
\institute[ETH]{$^{1}$Institute for Biomedical Engineering, ETH and University of Zurich \\ $^{2}$Preclinical Laboratory for Translational Research into Affective Disorders, DPPP, Psychiatric Hospital, University of Zurich}
\date{\today}

\newlength{\columnheight}
\setlength{\columnheight}{0.91\textheight}

\begin{document}
\begin{frame}
\begin{columns}
	\begin{column}{.4\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth}  % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Abstract}
						\input{abstract.tex}
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Methods}
						\vspace{.7em}
						\begin{figure}
							\centering\includegraphics[width=\textwidth]{img/og.png}
							\caption{\textbf{(a)} Optic fiber implant targeted at the dorsal raphe (DR), the ascending projections of which innervate numerous cortical and subcortical areas (adapted from \cite{Oegren2008}). Histological validation of the ChR2 construct expression: \textbf{(b)} localized to the DR  and \textbf{(c)} colocalized with serotonin (data from Saab and colleagues, unpublished).}
							\label{fig:og}
						\end{figure}
						
						\vspace{.4em}
						Transgenic ePet-Cre \cite{Scott2005} mice of a C57BL/6 backgroud were injected with a Cre-conditional channelrhodopsin (ChR2) expressing viral vector (Addgene plasmid \texttt{\#}20298), and the construct expression was validated histologically (see figure~\ref{fig:og}).
						
						\vspace{.4em}
						Animals were imaged over a longitudinal fluoxetine administration protocol, encompassing acute and chronic treatment sessions, and behavioral testing (as detailed in figure~\ref{fig:tdw}).
						
						\vspace{.4em}
						We performed fMRI at \SI{7}{\tesla}, using a spin-echo EPI sequence with a \SI{60}{\degree} flip angle, a repetition time of \SI{1}{\second}, and an echo time of \SI{5.913}{\milli\second}.
						Animals were administered an iron nanoparticle contrast agent (Endorem, Laboratoire Guebet SA) to produce cerebral blood volume (CBV) contrast and were placed under isoflurane/medetmidine anesthesia \cite{Grandjean2014}.
						The optogenetic stimulation protocol is depicted in figure~\ref{fig:sts}.

						\vspace{.8em}
						\py{pytex_fig('scripts/timetable.py', conf='poster/timetable.conf', label='tdw', caption='Per-subject longitudinal timetable. Shaded days indicate opto-fMRI measurement sessions (“naïve”, “acute”, “chronic/2w”, “chronic/4w”, “post” --- in order); blue and yellow colorized days indicate control and fluoxetine (\\SI{\\approx 20}{\\milli\\gram\\per\\kilo\\gram}) drinking water administration, respectively; blue and yellow dotted days indicate intravenous vehicle and fluoxetine (\\SI{10}{\\mg\\per\\kg}) administration, respectively; green and black dotted days indicate open field and forced swim test days, respectively (data not presented).')}
						
						\vspace{.8em}
						\py{pytex_fig('scripts/stimulation_ts.py', conf='poster/stimulation_ts.conf', label='sts', caption='Stimulation protocol summary and example cortical CBV timecourse. The red trace gives the mean of the CBV response in a cortical ROI (as in \\cref{fig:ca}); the dark blue trace shows the first level contrast regressor; the light blue trace shows a confound habituatuion regressor used to improve estimation; and the cyan colorized areas represent periods when the stimulus was applied.')}
						\vspace{-.5em}
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Quality Control}
						\begin{minipage}{.485\textwidth}
							\vspace{0.6em}
							\py{pytex_fig('scripts/weights.py', conf='poster/weights.conf', label='w', caption='Animal weights overview. Measurements correspond to the week preceding the session.')}
							\vspace{0.4em}
						\end{minipage}
						\begin{minipage}{.01\textwidth}~\end{minipage}
						\begin{minipage}{.485\textwidth}
							\vspace{0.6em}
							\py{pytex_fig('scripts/drinking.py', conf='poster/weights.conf', label='d', caption='Animal water intake overview. Measurements correspond to the week preceding the session.')}
							\vspace{0.4em}
						\end{minipage}
						
						We examine the animal weight and water consumption time courses for evidence of gross physiological fluoxetine administration effects (see \cref{fig:w,fig:d}), and find none.
						
						\vspace{0.4em}
						Additionally, we inspect the registration quality, to ascertain whether individual subject brains are not overfitted, and whether registration quality is impacted by the session.
						Figure~\ref{fig:rqc}, indeed, finds a significant subject effect and no significant session effect.
						
						\vspace{0.4em}
						\py{pytex_fig('scripts/registration_qc.py', conf='poster/registration_qc.conf', label='rqc', caption='Registration quality overview. Analysis of variance (ANOVA) indicates a significant subject effect [$F_{8,23}\\!=\\!6.6,\\, p\\!=\\!1\\!\\!\\times\\!\\!10^{-4}$] and no significant session effect [$F_{4,23}\\!=\\!0.94,\\, p\\!=\\!0.46$].')}
						\vspace{-.5em}
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Conclusions}
						\begin{itemize}
							\item 5-HT excitability increases following acute and chronic fluoxetine treatment.
							\item The response pattern to DR stimulation is not uniformly impacted by fluoxetine.
							\item 5-HT signalling is selectively potentiated by acute and chronic fluoxetine treatment.
							\item The cortical 5-HT signalling effect of fluoxetine treatment is likely multivariate.
							\item The cortical 5-HT signalling effect may be strongest for acute fluoxetine treatment.
							\item 5-HT excitability and signalling fully match controls upon fluoxetine withdrawal.
						\end{itemize}
					\end{myblock}\vfill
					\begin{myblock}{Outlook}
						\begin{itemize}
							\item The interpretation of \cref{fig:cpc} is non-trivial; multivariate pattern evaluation scores are almost universally positive (as in \cref{fig:pa}). This needs to be further investigated.
							\item As seen in \cref{fig:sts}, the CBV overshoot is not well modelled. Tackling this minor detail would increase statistic power across the board.
							\item Correlating the deviation-score map (\cref{fig:ap}) with gene expression patterns would help shed light on the mechanism underyling the multivariate response to fluoxetine.
						\end{itemize}
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
	\begin{column}{.6\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth} % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Stimulus Evoked Activity}
						\begin{center}
							\begin{minipage}{.58\textwidth}
							\vspace{.4em}
							\py{pytex_fig('scripts/drs_activity.py', conf='poster/sessions_large.conf', label='da', caption='Mean t-statistic values over an extended DR region of interest (overlain on figure). The null hypothesis that there is no difference in the session effect on the two levels of the treatment variable can be rejected [$F_{4,26}\\!=\\!7,\\, p\\!=\\!5\\!\\!\\times\\!\\!10^{-4}$].')}
							
							
								Stimulus evoked activity analysis shows a strong effect of fluoxetine treatment on DR excitability (see \cref{fig:da}).
								Assuming that activity in the serotonergic nuclei and its projection areas scales uniformly during fluoxetine treatment,
								one would expect to observe a similar profile when evaluating the multivariate naïve activation pattern across sessions, however, as per \cref{fig:pa}, this is not the case.

								\vspace{.4em}
								Though downstream activity is undoubtedly present in serotonergic projection areas (see \cref{fig:ca,fig:sts}), its direct analysis is difficult to interpret, as it is confounded by the variation in DR excitability documented in \cref{fig:da}.
								The network structure shown in \cref{fig:n} is best accounted for by seed-based connectivity analysis.
							\end{minipage}
							\begin{minipage}{.02\textwidth}~\end{minipage}
							\begin{minipage}{.37\textwidth}
								\vspace{.4em}
								\py{pytex_fig('scripts/pattern_activity.py', conf='poster/sessions_small.conf', label='pa', caption='Mean product of subject t-statistic maps and the unthresholded naïve condition t-statistic map (overlain on figure, shown thresholded at +/-3 for more clarity with respect to the template). The null hypothesis that there is no difference in the session effect on the two levels of the treatment variable cannot be rejected [$F_{4,26}\\!=\\!1.7,\\, p\\!=\\!0.19$].')}
								
								\py{pytex_fig('scripts/ctx_activity.py', conf='poster/sessions_small.conf', label='ca', caption='Mean t-statistic values over a cortical region of interest (overlain on figure). The null hypothesis that there is no difference in the session effect on the two levels of the treatment variable cannot be rejected [$F_{4,26}\\!=\\!1.8,\\, p\\!=\\!0.16$].')}
							\end{minipage}
						\end{center}
					\end{myblock}\vfill
					\begin{myblock}{Seed-Based Functional Connectivity}
						\begin{center}
							\begin{minipage}{.58\textwidth}
								\vspace{.7em}
								
								\textbf{N.B.:} Figures in this section do not represent stimulus evoked activity in the cortex (which is unsuited for meaningful interpretation) but signal coherence with respect to DR activity (corresponding to serotonergic signalling, as detailed in figure~\ref{fig:n}).
								
								\vspace{.4em}
								Analysis of signal coherence with the dorsal raphe in a cortical regoin of interest (see \cref{fig:cc}) fails to detect a significant effect of fluoxetine treatment on serotonergic signalling.
								A data-driven refinement of the region of interest involves determining which voxels show the highest deviation across the variable of interest, and subsequently visualizing their trajectory in order to determine whether the deviation may be characterized as a systematic effect.
								The resulting map (shown in full in \cref{fig:ap}) is here intersected with the aforementioned cortical region of interest to provide the required refinement.
							
								\vspace{.4em}
								The refined region of interest analysis (\cref{fig:cpmc}) shows a significant and coherent effect of fluoxetine treatment on serotonergic signalling in a subset of cortical areas.
								
								\vspace{.4em}
								The above analysis (as any ROI analysis) assumes a univariate effect (i.e. voxels with only one trajectory).
								Given the large extent of the cortex and the diffuse nature of serotonergic projections \cite{Oegren2008}, this may not be an appropriate constraint.
								In \cref{fig:cpc}, multivariate trajectories are more appropriately modelled, and a strong effect of fluoxetine treatment on serotonergic signalling to cortical projection areas (as weighted by signal deviation) emerges.
							\end{minipage}
							\begin{minipage}{.02\textwidth}~\end{minipage}
							\begin{minipage}{.37\textwidth}
								\vspace{.4em}
								\begin{figure}
									\centering\includedot[width=0.95\textwidth]{data/network_model}
									\caption{Simplified representation of signal transfer during optogenetic stimulation. Edge weight $u_1$ estimates serotonergic excitability, and weights $u_{2a}$, $u_{2b}$, $u_{2c}$, and $u_{2d}$ estimate serotonergic signalling.}
									\label{fig:n}
								\end{figure}
								\py{pytex_fig('scripts/ctx_drs_sfc.py', conf='poster/sessions_small.conf', label='cc', caption='Functional connectivity Z-scores for a DR seed region (shown in figure~\\ref{fig:da}) averaged over a cortical region of interest (overlain on figure). The null hypothesis that there is no difference in the session effect on the two levels of the treatment variable cannot be rejected [$F_{4,26}\\!=\\!0.39,\\, p\\!=\\!0.82$].')}
								
								\py{pytex_fig('scripts/ctx_pattern_mask_drs_sfc.py', conf='poster/sessions_small.conf', label='cpmc', caption='Functional connectivity Z-scores for a DR seed region (shown in figure~\\ref{fig:da}) averaged over a subset of the cortical region of interest (overlain on figure), reflecting the most strongly deviating voxels across the session variable (F-statistic threshold at 5). The null hypothesis that there is no difference in the session effect on the two levels of the treatment variable can be rejected [$F_{4,26}\\!=\\!2.7,\\, p\\!=\\!0.05$].')}
							\end{minipage}
						\end{center}
					\end{myblock}\vfill
					\begin{myblock}{ANOVA Pattern}
						\begin{center}
							\begin{minipage}{.63\textwidth}
								\vspace{.1em}
								\py{pytex_fig('scripts/anova_pattern.py', conf='poster/sessions_xlarge.conf', label='ap', caption='Z-statistic map of the F-contrast testing the presence of an interaction effect between the sesssion variable and the fluoxetine level of the treatment variable, specifically. The F-contrast evaluation was performed on the first level general linear model results.')}
							\end{minipage}
							\begin{minipage}{.02\textwidth}~\end{minipage}
							\begin{minipage}{.33\textwidth}
								\vspace{.1em}
								The map in \cref{fig:ap} shows a fair amount of symmetry, and reflects elements of the underlying anatomy.
								It bears, however, only a superficial resemblance to the naïve optogenetic response pattern seen in \cref{fig:pa}.
								Particularly, medial cortical voxels (as well as other voxels outside of the brain stem) are positively weighted, indicating that there is a spatially determined divergence in the serotonergic signalling response to the fluoxetine treatment --- which is consistent with \cref{fig:cpc}.

								\vspace{.4em}
								It is important to note that the map shown in \cref{fig:ap}, and from which the pattern in \cref{fig:cpc} was extracted, is not optimized for the F-contrast tested in the above two sections.
							\end{minipage}
						\end{center}
					\end{myblock}\vfill
                                        \begin{myblock}{References}
                                                \vspace{-0.8em}
                                                \begin{multicols}{3}
                                                        \tiny
                                                        \bibliographystyle{unsrt}
                                                        \bibliography{./bib}
                                                \end{multicols}
                                        \end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
\end{columns}
\scriptsize{This is an open source reproducible document. Download the source code from: \href{https://bitbucket.org/TheChymera/drlfom}{bitbucket.org/TheChymera/drlfom}}
\end{frame}
\end{document}
