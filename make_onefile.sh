#!/usr/bin/env bash

MASTER="article_s.tex"
OUTFILE="article_s_onefile.tex"

# Don't care if file is present
rm ${OUTFILE} 2> /dev/null

INPUT_PREFIX='\input{'
INPUT_SUFFIX='}'

while read -r line; do
    if [[ $line == '\input{'* ]]; then
	INFILE=${line#"$INPUT_PREFIX"}
	INFILE=${INFILE%"$INPUT_SUFFIX"}
	while read -r inline; do
	    echo $inline >> ${OUTFILE}
	done < ${INFILE}
    else
	echo "${line}" >> ${OUTFILE}
    fi
done < ${MASTER}
